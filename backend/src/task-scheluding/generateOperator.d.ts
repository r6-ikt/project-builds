import { OperatorDto } from "../operator/dto/operatorDto";
declare let todayOperator: OperatorDto | null;
export declare class TasksService {
    constructor();
    generateOperatorOnStartUp(): Promise<void>;
    generateOperatorEveryDay(): Promise<void>;
}
export declare function getOperators(): Promise<OperatorDto[]>;
export { todayOperator };
