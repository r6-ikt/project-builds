import { Strategy } from "passport-jwt";
import { JwtPayload } from "jsonwebtoken";
declare const RefreshStrategy_base: new (...args: any[]) => Strategy;
export declare class RefreshStrategy extends RefreshStrategy_base {
    constructor();
    validate(payload: JwtPayload): Promise<JwtPayload>;
    private static extractJWT;
}
export {};
