import { PrismaService } from "../../prisma/prisma.service";
import { OperatorDto } from "../../operator/dto/operatorDto";
import { OperatorCompareDto } from "../../operator/dto/operator-compareDto";
import { GuessResponseDto } from "../dto/guessResponse.dto";
import { RequestUserDto } from "../dto/requestUser.dto";
import { ReturnUserHistoryInterface } from "../dto/returnUserHistory.interface";
export declare let guessedOperator: OperatorDto;
export declare class GuessLogicService {
    private prismaService;
    constructor(prismaService: PrismaService);
    findGuessedOperator(guessedName: string, request: any, role: string): Promise<GuessResponseDto>;
    guestGuessValidation(guestToken: string): Promise<void>;
    userGuessValidation(requestUser: RequestUserDto): Promise<void>;
    getOperator(operatorName: string): Promise<void>;
    clientGuessedCorrectly(requestUser: RequestUserDto, request: any): Promise<ReturnUserHistoryInterface>;
    compareGuesses(obj1: OperatorCompareDto, obj2: OperatorCompareDto): boolean;
    getGuesses(user_id: number): Promise<GuessResponseDto[]>;
}
