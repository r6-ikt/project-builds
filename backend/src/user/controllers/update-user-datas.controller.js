"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUserDataController = void 0;
const common_1 = require("@nestjs/common");
const userRegister_dto_1 = require("../../auth/dto/userRegister.dto");
const update_user_datas_service_1 = require("../services/update-user-datas.service");
const UserUpdateWithPassword_1 = require("../../auth/dto/UserUpdateWithPassword");
const is_public_decorator_1 = require("../../custom decorator/is-public.decorator");
let UpdateUserDataController = class UpdateUserDataController {
    constructor(updateUserDataService) {
        this.updateUserDataService = updateUserDataService;
    }
    async updateUserWithoutPassword(updateUser, id) {
        return await this.updateUserDataService.updateUserWithoutPassword(updateUser, Number(id));
    }
    async updateUserWithPassword(updateUser, id) {
        return await this.updateUserDataService.updateUserWithPassword(updateUser, Number(id));
    }
};
exports.UpdateUserDataController = UpdateUserDataController;
__decorate([
    (0, common_1.Post)('/updateWithoutPassword/:id'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [userRegister_dto_1.UserRegisterDto, String]),
    __metadata("design:returntype", Promise)
], UpdateUserDataController.prototype, "updateUserWithoutPassword", null);
__decorate([
    (0, common_1.Post)('/updateWithPassword/:id'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [UserUpdateWithPassword_1.UserUpdateWithPassword, String]),
    __metadata("design:returntype", Promise)
], UpdateUserDataController.prototype, "updateUserWithPassword", null);
exports.UpdateUserDataController = UpdateUserDataController = __decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.Controller)('user'),
    __metadata("design:paramtypes", [update_user_datas_service_1.UpdateUserDataService])
], UpdateUserDataController);
//# sourceMappingURL=update-user-datas.controller.js.map