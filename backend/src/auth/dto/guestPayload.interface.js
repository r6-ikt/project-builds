"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuestPayloadInterface = void 0;
const swagger_1 = require("@nestjs/swagger");
class GuestPayloadInterface {
}
exports.GuestPayloadInterface = GuestPayloadInterface;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A Guest IP-címe!", example: "192.168.0.23", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "ip_address", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A Guest böngésző motorja!", example: "Yahoo", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "user_agent", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: false, description: "Az elérési útvonal!", example: "http://localhost:8000/", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "referrer", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: false, description: "A Guest neve a hálózaton!", example: "localhost", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "remote_hostname", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: false, description: "A Guest geolokációjának országa!", example: "Magyarország", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "country", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: false, description: "A Guest geolokációjának régiója!", example: "South Europe", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "region", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: false, description: "A Guest geolokációjának országa!", example: "Budapest", type: String }),
    __metadata("design:type", String)
], GuestPayloadInterface.prototype, "city", void 0);
//# sourceMappingURL=guestPayload.interface.js.map