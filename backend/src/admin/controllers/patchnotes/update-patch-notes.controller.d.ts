import { UpdatePatchNotesService } from "../../services/patchnotes/update-patch-notes/update-patch-notes.service";
import { UpdatePatchNotesDto } from "../../services/patchnotes/update-patch-notes/update-patch-notes.dto";
export declare class UpdatePatchNotesController {
    private updatePatchNoteService;
    constructor(updatePatchNoteService: UpdatePatchNotesService);
    updatePatchNotes(updatePatchNotes: UpdatePatchNotesDto): Promise<void>;
}
