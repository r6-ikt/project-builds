export interface disabledUserDTO {
    user_id: number;
    created_at: string;
    expire_at: string;
}
