export declare enum OperatorSquadEnum {
    NIGHTHAVEN = "nighthaven",
    VIPERSTRIKE = "viperstrike",
    REDHAMMER = "redhammer",
    WOLFGUARD = "wolfguard",
    GHOSTEYES = "ghosteyes"
}
