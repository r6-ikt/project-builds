"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOperatorsController = void 0;
const common_1 = require("@nestjs/common");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const get_operators_service_1 = require("../../services/operator/get-operators.service");
const swagger_1 = require("@nestjs/swagger");
const return_operator_dto_1 = require("../../dto/return-operator.dto");
let GetOperatorsController = class GetOperatorsController {
    constructor(getOperatorsService) {
        this.getOperatorsService = getOperatorsService;
    }
    getAllOperators() {
        return this.getOperatorsService.getAllOperators();
    }
    async getOperators(page, size, sort) {
        return this.getOperatorsService.getOperators(size, page, sort);
    }
};
exports.GetOperatorsController = GetOperatorsController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: return_operator_dto_1.returnOperatorDto, isArray: true, description: "Visszaadja az operator tábla teljes tartalmát!" }),
    (0, common_1.Get)('/getOperators'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GetOperatorsController.prototype, "getAllOperators", null);
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: get_operators_service_1.OperatorResponseProps, description: "Oldalanként adja át az összes Operator adatait!" }),
    (0, swagger_1.ApiParam)({ name: 'page', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'size', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'sort', type: 'string', required: true }),
    (0, common_1.Get)('/getOperators/page/:page'),
    __param(0, (0, common_1.Param)('page')),
    __param(1, (0, common_1.Query)('size')),
    __param(2, (0, common_1.Query)('sort')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String]),
    __metadata("design:returntype", Promise)
], GetOperatorsController.prototype, "getOperators", null);
exports.GetOperatorsController = GetOperatorsController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Operator'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [get_operators_service_1.GetOperatorsService])
], GetOperatorsController);
//# sourceMappingURL=get-operators.controller.js.map