import { LeaderboardService, UserResult } from './leaderboard.service';
import { UserIndexResult } from "./userIndexResult.interface";
export declare class LeaderboardController {
    private readonly leaderboardService;
    constructor(leaderboardService: LeaderboardService);
    getTop(topNumber: string): Promise<UserResult[]>;
    getUserFromLeaderBoard(username: string): Promise<UserIndexResult>;
}
