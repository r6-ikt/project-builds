export declare class UserRegisterDto {
    username: string;
    email: string;
    password: string;
}
