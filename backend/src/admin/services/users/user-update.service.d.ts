import { PrismaService } from "../../../prisma/prisma.service";
import { UpdateUserDto } from "../../dto/update-user.dto";
export declare class UserUpdateService {
    private prismaService;
    constructor(prismaService: PrismaService);
    updateUser(userUpdate: UpdateUserDto, user_id: number): Promise<{
        user_id: number;
        username: string;
        email: string;
        password: string;
        streak: number;
        point: number;
        admin: boolean;
        create_at: Date;
        last_login: Date;
        active: boolean;
    }>;
}
