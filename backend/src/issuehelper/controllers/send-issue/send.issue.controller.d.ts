import { SendIssueService } from "../../services/send-issue/send.issue.service";
import { ReturnIssueInterface } from "../../dto/returnIssue.interface";
import { IssueHelperDto } from "../../dto/issueHelper.dto";
export declare class SendIssueController {
    private readonly sendIssueService;
    constructor(sendIssueService: SendIssueService);
    sendIssue(issue: IssueHelperDto, req: any): Promise<ReturnIssueInterface>;
}
