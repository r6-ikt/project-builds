"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.formatDateWithOutTime = exports.formatDate = exports.CloseIssuesService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../prisma/prisma.service");
let CloseIssuesService = class CloseIssuesService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async closeAllIssue() {
        const count = await this.prismaService.issuehelper.count();
        if (count === 0) {
            throw new common_1.NotFoundException('No issues!');
        }
        const date = new Date(Date.now());
        const formattedDate = formatDate(date);
        this.prismaService.issuehelper.updateMany({
            data: {
                done_date: formattedDate,
            },
            where: {
                done_date: {
                    equals: null,
                }
            }
        });
    }
    async closeIssue(id) {
        const count = await this.prismaService.issuehelper.count({
            where: {
                issue_id: {
                    equals: id,
                }
            }
        });
        if (count === 0) {
            throw new common_1.NotFoundException('No such issue!');
        }
        const date = new Date(Date.now());
        const formattedDate = formatDate(date);
        return this.prismaService.issuehelper.update({
            data: {
                done_date: formattedDate,
            },
            where: {
                issue_id: id,
            }
        });
    }
};
exports.CloseIssuesService = CloseIssuesService;
exports.CloseIssuesService = CloseIssuesService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], CloseIssuesService);
function formatDate(date) {
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear();
    return `${year}-${month}-${day}T00:00:00.000Z`;
}
exports.formatDate = formatDate;
function formatDateWithOutTime(date) {
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const year = date.getFullYear();
    return `${year}-${month}-${day}`;
}
exports.formatDateWithOutTime = formatDateWithOutTime;
//# sourceMappingURL=close-issues.service.js.map