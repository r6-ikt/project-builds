"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsStringLengthBetween = void 0;
const class_validator_1 = require("class-validator");
function IsStringLengthBetween(min, max, validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            name: 'isStringLengthBetween',
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: {
                validate(value, args) {
                    const [minLength, maxLength] = [min, max];
                    return value.length >= minLength && value.length <= maxLength;
                },
                defaultMessage(args) {
                    const [minLength, maxLength] = [min, max];
                    return `$property must be between ${minLength} and ${maxLength} characters`;
                }
            }
        });
    };
}
exports.IsStringLengthBetween = IsStringLengthBetween;
//# sourceMappingURL=is-string-length-between.validator.js.map