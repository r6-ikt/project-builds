export interface RequestUserDto {
    username: string;
    email: string;
    admin: boolean;
    user_id: number;
    iat: number;
    exp: number;
}
