import { PrismaService } from "../prisma/prisma.service";
import { PrismaPromise } from "@prisma/client";
export declare class GuestService {
    private prismaService;
    constructor(prismaService: PrismaService);
    findGuestByToken(guest_token: string): PrismaPromise<{
        guest_token: string;
    }[]>;
}
