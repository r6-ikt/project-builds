export declare class OperatorDto {
    operator_id: number;
    name: string;
    active?: boolean;
    side: boolean;
    hp: number;
    speed: number;
    release_date: number;
    nationality: string;
    weapon1: string;
    weapon2?: string | undefined;
    squad: string;
}
