import { OperatorGetService } from "../services/operator-get.service";
import { PrismaPromise } from "@prisma/client";
import { ReturnOperatorInterface } from "../dto/returnOperator.interface";
export declare class OperatorController {
    private readonly operatorService;
    constructor(operatorService: OperatorGetService);
    getAllOperatorsName(): PrismaPromise<ReturnOperatorInterface[]>;
    getAllOperators(): import(".prisma/client").Prisma.PrismaPromise<{
        operator_id: number;
        name: string;
        side: boolean;
        hp: number;
        speed: number;
        release_date: number;
        nationality: string;
        weapon1: string;
        weapon2: string;
        squad: string;
        active: boolean;
    }[]>;
    getOperator(id: string): PrismaPromise<ReturnOperatorInterface>;
}
