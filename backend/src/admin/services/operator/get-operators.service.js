"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOperatorsService = exports.OperatorResponseProps = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const return_operator_dto_1 = require("../../dto/return-operator.dto");
const swagger_1 = require("@nestjs/swagger");
const prisma = new client_1.PrismaClient();
class OperatorResponseProps {
}
exports.OperatorResponseProps = OperatorResponseProps;
__decorate([
    (0, swagger_1.ApiProperty)({ type: return_operator_dto_1.returnOperatorDto, required: true, isArray: true, description: "Az operator tábla tartalma egy listában!", example: { operator_id: 1, name: 'Ace', side: true, hp: 1, speed: 3, release_date: 2016, nationality: 'German', weapon1: 'Assault Rifle', weapon2: 'SMG', squad: 'Nighthaven' } }),
    __metadata("design:type", Array)
], OperatorResponseProps.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az adatbázis sorainak száma!", example: 12, type: Number }),
    __metadata("design:type", Number)
], OperatorResponseProps.prototype, "rowCount", void 0);
let GetOperatorsService = class GetOperatorsService {
    getAllOperators() {
        try {
            return prisma.operators.findMany();
        }
        catch (error) {
            throw new common_1.NotFoundException(error);
        }
    }
    async getOperators(size, page, sort) {
        const sortParams = sort.split(',');
        const field = sortParams[0];
        const sortMode = sortParams[1];
        const perPage = size;
        const offset = (page) * perPage;
        let orderBy = {};
        if (sort) {
            orderBy = {
                [field]: sortMode
            };
        }
        const result = await prisma.operators.findMany({
            take: perPage,
            skip: offset,
            orderBy,
        });
        const allRowCount = await prisma.operators.count();
        const response = { rows: result, rowCount: allRowCount };
        return response;
    }
};
exports.GetOperatorsService = GetOperatorsService;
exports.GetOperatorsService = GetOperatorsService = __decorate([
    (0, common_1.Injectable)()
], GetOperatorsService);
//# sourceMappingURL=get-operators.service.js.map