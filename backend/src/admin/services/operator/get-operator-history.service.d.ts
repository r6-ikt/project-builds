import { PrismaService } from "../../../prisma/prisma.service";
import { history_operator } from "@prisma/client";
import { historyOperatorDto } from "../../dto/history-operator.dto";
export declare class HistoryOperatorResponseProps {
    rows: historyOperatorDto[];
    rowCount: number;
}
export declare class GetOperatorHistoryService {
    private prismaService;
    constructor(prismaService: PrismaService);
    getOperatorHistory(): Promise<history_operator[]>;
    getOperatorHistoryByPages(size: number, page: number, sort: string): Promise<HistoryOperatorResponseProps>;
}
