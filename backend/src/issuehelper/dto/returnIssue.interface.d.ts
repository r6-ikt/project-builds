export interface ReturnIssueInterface {
    issue_id: number;
    user_id: number;
    send_date: Date;
    done_date: Date;
    complaint: string;
}
