import { PrismaService } from "../prisma/prisma.service";
import { UserIndexResult } from "./userIndexResult.interface";
export type UserResult = Omit<UserIndexResult, 'index'>;
export declare class LeaderboardService {
    private prisma;
    constructor(prisma: PrismaService);
    getTop(topNumber: string): Promise<UserResult[]>;
    getUserFromLeaderBoard(username: string): Promise<UserIndexResult>;
}
