"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorUpdateController = void 0;
const common_1 = require("@nestjs/common");
const operator_update_service_1 = require("../../services/operator/operator-update.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const return_operator_dto_1 = require("../../dto/return-operator.dto");
const jwt_1 = require("@nestjs/jwt");
const swagger_1 = require("@nestjs/swagger");
const operatorDto_1 = require("../../../operator/dto/operatorDto");
let OperatorUpdateController = class OperatorUpdateController {
    constructor(operatorUpdateService, jwtService) {
        this.operatorUpdateService = operatorUpdateService;
        this.jwtService = jwtService;
    }
    updateOperator(updateOperator, id, request) {
        const reqHeader = request.headers['authorization'];
        let token = '';
        if (reqHeader && reqHeader.startsWith('Bearer ')) {
            token = reqHeader.substring(7);
        }
        else {
            throw new common_1.UnauthorizedException("Access Denied!");
        }
        if (token === '') {
            throw new common_1.NotFoundException("Cannot find access token!");
        }
        const decodeToken = this.jwtService.decode(token);
        return this.operatorUpdateService.updateOperator(updateOperator, Number(id), Number(decodeToken.user_id));
    }
};
exports.OperatorUpdateController = OperatorUpdateController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 201, type: return_operator_dto_1.returnOperatorDto, description: "Frissíti a kivánt Operator-t, majd visszaadja az egész objektumot!" }),
    (0, swagger_1.ApiParam)({ name: 'page', type: 'number', required: true, description: "A frissíteni kivánt Operator azonosítója!" }),
    (0, swagger_1.ApiBody)({ type: operatorDto_1.OperatorDto, description: 'Operator frissítésének beviteli értéke!' }),
    (0, common_1.Post)('/update-operator/:id'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __param(2, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, Object]),
    __metadata("design:returntype", Promise)
], OperatorUpdateController.prototype, "updateOperator", null);
exports.OperatorUpdateController = OperatorUpdateController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Operator'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [operator_update_service_1.OperatorUpdateService, jwt_1.JwtService])
], OperatorUpdateController);
//# sourceMappingURL=operator-update.controller.js.map