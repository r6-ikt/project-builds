export declare class OperatorCompareDto {
    side: boolean;
    hp: boolean;
    speed: boolean;
    release_date: number;
    nationality: boolean;
    weapon1: boolean;
    weapon2?: boolean;
    squad: boolean;
}
