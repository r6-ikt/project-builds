import { PrismaService } from "../../../prisma/prisma.service";
export declare class DeleteIssueService {
    private prismaService;
    constructor(prismaService: PrismaService);
    deleteAllIssue(): Promise<void>;
    deleteIssue(id: number): Promise<void>;
}
