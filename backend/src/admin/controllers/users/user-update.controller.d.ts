import { UserUpdateService } from "../../services/users/user-update.service";
import { UpdateUserDto } from "../../dto/update-user.dto";
export declare class UserUpdateController {
    private readonly userUpdateService;
    constructor(userUpdateService: UserUpdateService);
    updateUser(updateUser: UpdateUserDto, id: string): Promise<{
        user_id: number;
        username: string;
        email: string;
        password: string;
        streak: number;
        point: number;
        admin: boolean;
        create_at: Date;
        last_login: Date;
        active: boolean;
    }>;
}
