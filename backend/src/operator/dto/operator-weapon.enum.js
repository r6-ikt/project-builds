"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorWeaponEnum = void 0;
var OperatorWeaponEnum;
(function (OperatorWeaponEnum) {
    OperatorWeaponEnum["ASSAULT_RIFLE"] = "assault rifle";
    OperatorWeaponEnum["MARKSMAN_RIFLE"] = "marksman rifle";
    OperatorWeaponEnum["SUBMACHINE_GUN"] = "submachine gun";
    OperatorWeaponEnum["LIGHT_MACHINE_GUN"] = "light machine gun";
    OperatorWeaponEnum["SHOTGUN"] = "shotgun";
})(OperatorWeaponEnum || (exports.OperatorWeaponEnum = OperatorWeaponEnum = {}));
//# sourceMappingURL=operator-weapon.enum.js.map