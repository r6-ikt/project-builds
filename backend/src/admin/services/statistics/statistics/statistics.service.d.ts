import { PrismaService } from "../../../../prisma/prisma.service";
export declare class StatisticsService {
    private prismaService;
    constructor(prismaService: PrismaService);
    getHistoryRegisterStatistics(dates: Date[], limit: Date, today: Date): Promise<{
        count: number;
        create_date: Date;
    }[]>;
    getHistoryUserStatistics(limit: Date, today: Date, dates: Date[]): Promise<{
        history_date: Date;
        count: number;
    }[]>;
    getLastSevenDays(): Promise<{
        historyCorrectGuesses: {
            history_date: Date;
            count: number;
        }[];
        historyRegisterStatistics: {
            create_date: Date;
            count: number;
        }[];
    }>;
}
