import { GetOperatorsService, OperatorResponseProps } from "../../services/operator/get-operators.service";
import { returnOperatorDto } from "../../dto/return-operator.dto";
export declare class GetOperatorsController {
    private readonly getOperatorsService;
    constructor(getOperatorsService: GetOperatorsService);
    getAllOperators(): Promise<returnOperatorDto[]>;
    getOperators(page: number, size: number, sort: string): Promise<OperatorResponseProps>;
}
