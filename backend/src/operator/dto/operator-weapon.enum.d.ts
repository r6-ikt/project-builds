export declare enum OperatorWeaponEnum {
    ASSAULT_RIFLE = "assault rifle",
    MARKSMAN_RIFLE = "marksman rifle",
    SUBMACHINE_GUN = "submachine gun",
    LIGHT_MACHINE_GUN = "light machine gun",
    SHOTGUN = "shotgun"
}
