"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeletePatchNotesController = void 0;
const common_1 = require("@nestjs/common");
const delete_patch_notes_service_1 = require("../../services/patchnotes/delete-patch-notes/delete-patch-notes.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const swagger_1 = require("@nestjs/swagger");
let DeletePatchNotesController = class DeletePatchNotesController {
    constructor(DeletePatchNoteService) {
        this.DeletePatchNoteService = DeletePatchNoteService;
    }
    async deletePatchNotes(id) {
        return await this.DeletePatchNoteService.deletePatchNote(Number(id));
    }
};
exports.DeletePatchNotesController = DeletePatchNotesController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, description: "Töröl egy patch-note-ot, azonosító alapján!" }),
    (0, swagger_1.ApiParam)({ name: 'id', type: 'number', required: true }),
    (0, common_1.Post)('delete-patch-notes/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeletePatchNotesController.prototype, "deletePatchNotes", null);
exports.DeletePatchNotesController = DeletePatchNotesController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'PatchNote'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [delete_patch_notes_service_1.DeletePatchNotesService])
], DeletePatchNotesController);
//# sourceMappingURL=delete-patch-notes.controller.js.map