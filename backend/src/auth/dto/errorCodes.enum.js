"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorCodes = void 0;
var ErrorCodes;
(function (ErrorCodes) {
    ErrorCodes["USERNAME_ALREADY_EXISTS"] = "This username is already taken!";
    ErrorCodes["EMAIL_ALREADY_EXITS"] = "This email is already taken!";
    ErrorCodes["INVALID_EMAIL_FORMAT"] = "Invalid email format";
    ErrorCodes["WRONG_PASSWORD"] = "The password must be at least eight characters long and contain at least one uppercase letter and one digit";
})(ErrorCodes || (exports.ErrorCodes = ErrorCodes = {}));
//# sourceMappingURL=errorCodes.enum.js.map