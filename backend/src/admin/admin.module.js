"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminModule = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const get_issues_service_1 = require("./services/issues/get-issues.service");
const get_issues_controller_1 = require("./controllers/issues/get-issues.controller");
const close_issues_service_1 = require("./services/issues/close-issues.service");
const close_issues_controller_1 = require("./controllers/issues/close-issues.controller");
const delete_issues_service_1 = require("./services/issues/delete-issues.service");
const delete_issues_controller_1 = require("./controllers/issues/delete-issues.controller");
const set_inactive_user_service_1 = require("./services/users/set-inactive-user.service");
const set_inactive_user_controller_1 = require("./controllers/users/set-inactive-user.controller");
const admin_role_for_user_service_1 = require("./services/users/admin-role-for-user.service");
const admin_role_for_user_controller_1 = require("./controllers/users/admin-role-for-user.controller");
const operator_create_service_1 = require("./services/operator/operator-create.service");
const operator_create_controller_1 = require("./controllers/operator/operator-create.controller");
const get_operator_history_service_1 = require("./services/operator/get-operator-history.service");
const get_operator_history_controller_1 = require("./controllers/operator/get-operator-history.controller");
const operator_update_service_1 = require("./services/operator/operator-update.service");
const operator_update_controller_1 = require("./controllers/operator/operator-update.controller");
const set_active_user_service_1 = require("./services/users/set-active-user.service");
const set_active_user_controller_1 = require("./controllers/users/set-active-user.controller");
const user_role_for_admin_controller_1 = require("./controllers/users/user-role-for-admin.controller");
const user_role_for_admin_service_1 = require("./services/users/user-role-for-admin.service");
const patch_note_module_1 = require("../patch-note/patch-note.module");
const patch_note_service_1 = require("../patch-note/patch-note.service");
const get_patch_notes_controller_1 = require("./controllers/patchnotes/get-patch-notes.controller");
const create_patch_notes_controller_1 = require("./controllers/patchnotes/create-patch-notes.controller");
const create_patch_notes_service_1 = require("./services/patchnotes/create-patch-notes/create-patch-notes.service");
const get_all_user_service_1 = require("./services/users/get-all-user.service");
const get_all_user_controller_1 = require("./controllers/users/get-all-user.controller");
const get_today_operator_service_1 = require("./services/operator/get-today-operator.service");
const get_today_operator_controller_1 = require("./controllers/operator/get-today-operator.controller");
const jwt_1 = require("@nestjs/jwt");
const update_patch_notes_controller_1 = require("./controllers/patchnotes/update-patch-notes.controller");
const update_patch_notes_service_1 = require("./services/patchnotes/update-patch-notes/update-patch-notes.service");
const get_operators_service_1 = require("./services/operator/get-operators.service");
const get_operators_controller_1 = require("./controllers/operator/get-operators.controller");
const user_update_service_1 = require("./services/users/user-update.service");
const user_update_controller_1 = require("./controllers/users/user-update.controller");
const statistics_controller_1 = require("./controllers/statistics/statistics.controller");
const statistics_service_1 = require("./services/statistics/statistics/statistics.service");
const delete_patch_notes_controller_1 = require("./controllers/patchnotes/delete-patch-notes.controller");
const delete_patch_notes_service_1 = require("./services/patchnotes/delete-patch-notes/delete-patch-notes.service");
let AdminModule = class AdminModule {
};
exports.AdminModule = AdminModule;
exports.AdminModule = AdminModule = __decorate([
    (0, common_1.Module)({
        providers: [prisma_service_1.PrismaService,
            get_issues_service_1.GetIssuesService,
            close_issues_service_1.CloseIssuesService,
            delete_issues_service_1.DeleteIssueService,
            set_inactive_user_service_1.SetInactiveUserService,
            set_active_user_service_1.SetActiveUserService,
            admin_role_for_user_service_1.AdminRoleForUserService,
            user_role_for_admin_service_1.UserRoleForAdminService,
            get_all_user_service_1.GetAllUserService,
            user_update_service_1.UserUpdateService,
            operator_create_service_1.OperatorCreateService,
            get_operator_history_service_1.GetOperatorHistoryService,
            operator_update_service_1.OperatorUpdateService,
            patch_note_service_1.PatchNoteService,
            create_patch_notes_service_1.CreatePatchNotesService,
            get_today_operator_service_1.GetTodayOperatorService,
            get_operators_service_1.GetOperatorsService,
            jwt_1.JwtService,
            update_patch_notes_service_1.UpdatePatchNotesService,
            statistics_service_1.StatisticsService,
            delete_patch_notes_service_1.DeletePatchNotesService,
        ],
        controllers: [get_issues_controller_1.GetIssuesController,
            close_issues_controller_1.CloseIssuesController,
            delete_issues_controller_1.DeleteIssueController,
            set_inactive_user_controller_1.SetInactiveUserController,
            set_active_user_controller_1.SetActiveUserController,
            admin_role_for_user_controller_1.AdminRoleForUserController,
            user_role_for_admin_controller_1.UserRoleForAdminController,
            get_all_user_controller_1.GetAllUserController,
            user_update_controller_1.UserUpdateController,
            operator_create_controller_1.OperatorCreateController,
            get_operator_history_controller_1.GetOperatorHistoryController,
            operator_update_controller_1.OperatorUpdateController,
            get_patch_notes_controller_1.GetPatchNotesController,
            create_patch_notes_controller_1.CreatePatchNotesController,
            get_today_operator_controller_1.GetTodayOperatorController,
            get_operators_controller_1.GetOperatorsController,
            update_patch_notes_controller_1.UpdatePatchNotesController,
            statistics_controller_1.StatisticsController,
            delete_patch_notes_controller_1.DeletePatchNotesController,
        ],
        imports: [
            patch_note_module_1.PatchNoteModule,
            jwt_1.JwtModule,
        ],
    })
], AdminModule);
//# sourceMappingURL=admin.module.js.map