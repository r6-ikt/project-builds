"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeletePatchNotesService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../../prisma/prisma.service");
let DeletePatchNotesService = class DeletePatchNotesService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async deletePatchNote(id) {
        try {
            await this.prismaService.patch_note.delete({
                where: {
                    patch_id: id,
                },
            });
        }
        catch (e) {
            throw new common_1.BadRequestException('No such patch note with this id: ' + id);
        }
    }
};
exports.DeletePatchNotesService = DeletePatchNotesService;
exports.DeletePatchNotesService = DeletePatchNotesService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], DeletePatchNotesService);
//# sourceMappingURL=delete-patch-notes.service.js.map