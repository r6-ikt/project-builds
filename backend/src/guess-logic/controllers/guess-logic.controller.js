"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuessLogicController = void 0;
const common_1 = require("@nestjs/common");
const guess_logic_service_1 = require("../services/guess-logic.service");
const is_public_decorator_1 = require("../../custom decorator/is-public.decorator");
const is_guest_decorator_1 = require("../../custom decorator/is-guest.decorator");
let GuessLogicController = class GuessLogicController {
    constructor(guessLogicService) {
        this.guessLogicService = guessLogicService;
    }
    async findGuessedOperator(guessedOperator, req) {
        const role = 'user';
        return this.guessLogicService.findGuessedOperator(guessedOperator.toLowerCase(), req, role);
    }
    async getGuessesById(req) {
        return this.guessLogicService.getGuesses(req.user.user_id);
    }
    findGuessedOperatorGuest(request, operatorName) {
        const guestToken = request.cookies['guest_token'];
        const role = 'guest';
        if (guestToken) {
            return this.guessLogicService.findGuessedOperator(operatorName.toLowerCase(), request, role);
        }
        else {
            throw new common_1.UnauthorizedException('Invalid guest token!');
        }
    }
};
exports.GuessLogicController = GuessLogicController;
__decorate([
    (0, common_1.Post)('/:operatorName'),
    __param(0, (0, common_1.Param)('operatorName')),
    __param(1, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], GuessLogicController.prototype, "findGuessedOperator", null);
__decorate([
    (0, common_1.Get)('/guesses'),
    __param(0, (0, common_1.Request)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GuessLogicController.prototype, "getGuessesById", null);
__decorate([
    (0, is_public_decorator_1.Public)(),
    (0, is_guest_decorator_1.Guest)(),
    (0, common_1.Post)('guest/:operatorName'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('operatorName')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], GuessLogicController.prototype, "findGuessedOperatorGuest", null);
exports.GuessLogicController = GuessLogicController = __decorate([
    (0, common_1.Controller)('guess-logic'),
    __metadata("design:paramtypes", [guess_logic_service_1.GuessLogicService])
], GuessLogicController);
//# sourceMappingURL=guess-logic.controller.js.map