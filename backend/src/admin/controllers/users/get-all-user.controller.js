"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllUserController = void 0;
const common_1 = require("@nestjs/common");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const get_all_user_service_1 = require("../../services/users/get-all-user.service");
const swagger_1 = require("@nestjs/swagger");
const return_user_dto_1 = require("../../dto/return-user.dto");
let GetAllUserController = class GetAllUserController {
    constructor(getAllUserService) {
        this.getAllUserService = getAllUserService;
    }
    getAllUsers() {
        return this.getAllUserService.getUsers();
    }
    async getAllUsersByPage(page, size, sort) {
        return this.getAllUserService.getAllUsers(size, page, sort);
    }
};
exports.GetAllUserController = GetAllUserController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: return_user_dto_1.returnUserDto, isArray: true, description: "Visszaadja a Users tábla tartalmát, kivéve jelszavakat!" }),
    (0, common_1.Get)('/users'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GetAllUserController.prototype, "getAllUsers", null);
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: get_all_user_service_1.UserResponseProps, description: "Oldalanként adja át az összes User adatait, kivéve a jelszavát!" }),
    (0, swagger_1.ApiParam)({ name: 'page', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'size', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'sort', type: 'string', required: true }),
    (0, common_1.Get)('/users/page/:page'),
    __param(0, (0, common_1.Param)('page')),
    __param(1, (0, common_1.Query)('size')),
    __param(2, (0, common_1.Query)('sort')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String]),
    __metadata("design:returntype", Promise)
], GetAllUserController.prototype, "getAllUsersByPage", null);
exports.GetAllUserController = GetAllUserController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Users'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [get_all_user_service_1.GetAllUserService])
], GetAllUserController);
//# sourceMappingURL=get-all-user.controller.js.map