import { PrismaService } from "../../../prisma/prisma.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class SetInactiveUserService {
    private prismaService;
    constructor(prismaService: PrismaService);
    setInactiveUser(id: number): Promise<returnUserDto>;
}
