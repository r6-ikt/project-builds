"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../services/auth.service");
const auth_controller_1 = require("../controllers/auth.controller");
const local_strategy_1 = require("../strategies/local-strategy");
const prisma_service_1 = require("../../prisma/prisma.service");
const jwt_strategy_1 = require("../strategies/jwt-strategy");
const user_service_1 = require("../../user/services/user.service");
const jwt_1 = require("@nestjs/jwt");
const user_module_1 = require("../../user/modules/user.module");
const guest_module_1 = require("../../guest/guest.module");
const guest_service_1 = require("../../guest/guest.service");
const refresh_strategy_1 = require("../strategies/refresh-strategy");
let AuthModule = class AuthModule {
};
exports.AuthModule = AuthModule;
exports.AuthModule = AuthModule = __decorate([
    (0, common_1.Module)({
        providers: [auth_service_1.AuthService, local_strategy_1.LocalStrategy, prisma_service_1.PrismaService, jwt_strategy_1.JwtStrategy, jwt_1.JwtService, user_service_1.UserService, guest_service_1.GuestService, refresh_strategy_1.RefreshStrategy],
        controllers: [auth_controller_1.AuthController],
        exports: [auth_service_1.AuthService],
        imports: [(0, common_1.forwardRef)(() => guest_module_1.GuestModule), (0, common_1.forwardRef)(() => user_module_1.UserModule)]
    })
], AuthModule);
//# sourceMappingURL=auth.module.js.map