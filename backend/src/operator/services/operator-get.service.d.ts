import { Prisma, PrismaPromise } from "@prisma/client";
import { ReturnOperatorInterface } from "../dto/returnOperator.interface";
export declare class OperatorGetService {
    getAllOperatorsName(): PrismaPromise<ReturnOperatorInterface[]>;
    getAllOperators(): Prisma.PrismaPromise<{
        operator_id: number;
        name: string;
        side: boolean;
        hp: number;
        speed: number;
        release_date: number;
        nationality: string;
        weapon1: string;
        weapon2: string;
        squad: string;
        active: boolean;
    }[]>;
    getOperator(id: number): PrismaPromise<ReturnOperatorInterface>;
}
