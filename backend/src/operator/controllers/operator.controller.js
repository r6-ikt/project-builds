"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorController = void 0;
const common_1 = require("@nestjs/common");
const operator_get_service_1 = require("../services/operator-get.service");
const is_public_decorator_1 = require("../../custom decorator/is-public.decorator");
let OperatorController = class OperatorController {
    constructor(operatorService) {
        this.operatorService = operatorService;
    }
    getAllOperatorsName() {
        return this.operatorService.getAllOperatorsName();
    }
    getAllOperators() {
        return this.operatorService.getAllOperators();
    }
    getOperator(id) {
        return this.operatorService.getOperator(Number(id));
    }
};
exports.OperatorController = OperatorController;
__decorate([
    (0, common_1.Get)('/'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], OperatorController.prototype, "getAllOperatorsName", null);
__decorate([
    (0, common_1.Get)('/all'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], OperatorController.prototype, "getAllOperators", null);
__decorate([
    (0, common_1.Get)('/:param'),
    __param(0, (0, common_1.Param)('param')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Object)
], OperatorController.prototype, "getOperator", null);
exports.OperatorController = OperatorController = __decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.Controller)('/operators'),
    __metadata("design:paramtypes", [operator_get_service_1.OperatorGetService])
], OperatorController);
//# sourceMappingURL=operator.controller.js.map