import { StatisticsService } from "../../services/statistics/statistics/statistics.service";
export declare class StatisticsController {
    private statisticsService;
    constructor(statisticsService: StatisticsService);
    getLastSevenDays(): Promise<{
        historyCorrectGuesses: {
            history_date: Date;
            count: number;
        }[];
        historyRegisterStatistics: {
            create_date: Date;
            count: number;
        }[];
    }>;
}
