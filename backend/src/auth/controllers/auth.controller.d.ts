/// <reference types="cookie-parser" />
import { AuthService } from "../services/auth.service";
import { UserRegisterDto } from "../dto/userRegister.dto";
import { ReturnTokenInterface } from "../dto/returnToken.interface";
import { Request, Response } from 'express';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    login(req: any, response: Response): Promise<ReturnTokenInterface>;
    logout(response: Response): Promise<ReturnTokenInterface>;
    postRegister(registerUser: UserRegisterDto): Promise<void>;
    refreshToken(req: Request, response: Response): Promise<{
        accessToken: string;
    }>;
    guestAuth(request: Request, response: Response): Promise<{
        guest_token: string;
    }>;
}
