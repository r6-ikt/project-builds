"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtAuthGuard = void 0;
const passport_1 = require("@nestjs/passport");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const is_public_decorator_1 = require("../../custom decorator/is-public.decorator");
const is_admin_decorator_1 = require("../../custom decorator/is-admin.decorator");
const jwt = require("jsonwebtoken");
const is_guest_decorator_1 = require("../../custom decorator/is-guest.decorator");
let JwtAuthGuard = class JwtAuthGuard extends (0, passport_1.AuthGuard)('jwt') {
    constructor(reflector) {
        super();
        this.reflector = reflector;
    }
    canActivate(context) {
        const isPublic = this.reflector.getAllAndOverride(is_public_decorator_1.IS_PUBLIC_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        const isAdmin = this.reflector.get(is_admin_decorator_1.IS_ADMIN_KEY, context.getClass());
        const isGuest = this.reflector.get(is_guest_decorator_1.IS_GUEST_KEY, context.getHandler());
        if (isPublic) {
            if (isGuest) {
                const request = context.switchToHttp().getRequest();
                const accessToken = request.cookies['ac'];
                if (accessToken) {
                    return request.guestToken = accessToken;
                }
            }
            return true;
        }
        if (isAdmin) {
            const request = context.switchToHttp().getRequest();
            if (!request.cookies.ac) {
                throw new common_1.ForbiddenException('Access Token not provided!');
            }
            const accessToken = request.cookies.ac;
            const decodedToken = jwt.decode(accessToken);
            if (decodedToken && decodedToken.admin === true) {
                return true;
            }
            else {
                throw new common_1.ForbiddenException("Access Denied!");
            }
        }
        return super.canActivate(context);
    }
};
exports.JwtAuthGuard = JwtAuthGuard;
exports.JwtAuthGuard = JwtAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [core_1.Reflector])
], JwtAuthGuard);
//# sourceMappingURL=jwt-auth.guard.js.map