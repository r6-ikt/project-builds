import { AdminRoleForUserService } from "../../services/users/admin-role-for-user.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class AdminRoleForUserController {
    private readonly createAdminFromUserService;
    constructor(createAdminFromUserService: AdminRoleForUserService);
    changeToAdmin(id: string): Promise<returnUserDto>;
}
