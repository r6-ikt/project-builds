"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../services/auth.service");
const local_auth_guard_1 = require("../guards/local-auth.guard");
const userRegister_dto_1 = require("../dto/userRegister.dto");
const is_public_decorator_1 = require("../../custom decorator/is-public.decorator");
const axios_1 = require("axios");
const refresh_auth_guard_1 = require("../guards/refresh-auth.guard");
let AuthController = class AuthController {
    constructor(authService) {
        this.authService = authService;
    }
    async login(req, response) {
        const tok = await this.authService.login(req.user);
        response.clearCookie('ac');
        response.clearCookie('refreshToken');
        response.cookie('ac', tok.accessToken, {
            httpOnly: true,
            expires: new Date(2322, 1, 1)
        });
        response.cookie('refreshToken', tok.refreshToken, { httpOnly: true });
        return tok;
    }
    async logout(response) {
        response.clearCookie('ac');
        response.clearCookie('refreshToken');
        return;
    }
    async postRegister(registerUser) {
        return await this.authService.CreateUser(registerUser);
    }
    async refreshToken(req, response) {
        const refreshToken = req.cookies['refreshToken'];
        if (!refreshToken) {
            throw new common_1.NotAcceptableException('Invalid authorization token!');
        }
        const token = await this.authService.refreshToken(refreshToken);
        response.clearCookie('ac');
        response.cookie('ac', token.accessToken, { httpOnly: true, expires: new Date(2322, 1, 1) });
        return token;
    }
    async guestAuth(request, response) {
        const ipAddress = request.ip;
        const userAgent = request.headers['user-agent'];
        const referrer = request.headers['referer'];
        const remoteHostname = request.hostname;
        const geoLocationResponse = await axios_1.default.get(`https://ipinfo.io/${ipAddress}/json?token=76e3f2537c543a`);
        const geoLocationData = geoLocationResponse.data;
        const payload = { ip_address: ipAddress, user_agent: userAgent, referrer: referrer, remote_hostname: remoteHostname, country: geoLocationData.country, region: geoLocationData.region, city: geoLocationData.city };
        return this.authService.guestToken(payload, response);
    }
};
exports.AuthController = AuthController;
__decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.UseGuards)(local_auth_guard_1.LocalAuthGuard),
    (0, common_1.Post)('login'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
__decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.Post)('logout'),
    __param(0, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "logout", null);
__decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.Post)('register'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [userRegister_dto_1.UserRegisterDto]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "postRegister", null);
__decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.UseGuards)(refresh_auth_guard_1.RefreshAuthGuard),
    (0, common_1.Post)('refresh'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "refreshToken", null);
__decorate([
    (0, is_public_decorator_1.Public)(),
    (0, common_1.Get)('guest'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "guestAuth", null);
exports.AuthController = AuthController = __decorate([
    (0, common_1.Controller)('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthController);
//# sourceMappingURL=auth.controller.js.map