export interface ReturnTokenInterface {
    accessToken: string;
    refreshToken: string;
}
