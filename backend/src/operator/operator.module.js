"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorModule = void 0;
const common_1 = require("@nestjs/common");
const operator_get_service_1 = require("./services/operator-get.service");
const operator_controller_1 = require("./controllers/operator.controller");
const generateOperator_1 = require("../task-scheluding/generateOperator");
let OperatorModule = class OperatorModule {
};
exports.OperatorModule = OperatorModule;
exports.OperatorModule = OperatorModule = __decorate([
    (0, common_1.Module)({
        providers: [operator_get_service_1.OperatorGetService, generateOperator_1.TasksService],
        controllers: [operator_controller_1.OperatorController]
    })
], OperatorModule);
//# sourceMappingURL=operator.module.js.map