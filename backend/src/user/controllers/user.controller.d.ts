/// <reference types="multer" />
import { Response } from "express";
import { UserService } from '../services/user.service';
import { ReturnUserByNameInterface } from "../../auth/dto/returnUserByName.Interface";
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    getUserData(request: any): Promise<ReturnUserByNameInterface>;
    getImage(res: Response, id: string): Promise<void>;
    uploadFile(file: Express.Multer.File, id: string, res: Response): Promise<void>;
}
