export declare class UpdatePatchNotesDto {
    patch_id: number;
    operator_id: number;
    admin_id: number;
    updated_field: string;
    previous_value: string;
    new_value: string;
    patch_date: string;
}
