export declare class UserUpdateWithPassword {
    username: string;
    email: string;
    password: string;
    newPassword: string;
}
