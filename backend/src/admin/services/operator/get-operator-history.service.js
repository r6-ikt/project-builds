"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOperatorHistoryService = exports.HistoryOperatorResponseProps = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../prisma/prisma.service");
const history_operator_dto_1 = require("../../dto/history-operator.dto");
const swagger_1 = require("@nestjs/swagger");
class HistoryOperatorResponseProps {
}
exports.HistoryOperatorResponseProps = HistoryOperatorResponseProps;
__decorate([
    (0, swagger_1.ApiProperty)({ type: history_operator_dto_1.historyOperatorDto, required: true, isArray: true, description: "A history_operator tartalma egy listában!", example: { id: 1, operator_id: 1, history_date: "2024-04-06" } }),
    __metadata("design:type", Array)
], HistoryOperatorResponseProps.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az adatbázis sorainak száma!", example: 12, type: Number }),
    __metadata("design:type", Number)
], HistoryOperatorResponseProps.prototype, "rowCount", void 0);
let GetOperatorHistoryService = class GetOperatorHistoryService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    getOperatorHistory() {
        return this.prismaService.history_operator.findMany();
    }
    async getOperatorHistoryByPages(size, page, sort) {
        const sortParams = sort.split(',');
        const field = sortParams[0];
        const sortMode = sortParams[1];
        const perPage = size;
        const offset = (page) * perPage;
        let orderBy = {};
        if (sort) {
            orderBy = {
                [field]: sortMode
            };
        }
        const result = await this.prismaService.history_operator.findMany({
            take: perPage,
            skip: offset,
            orderBy,
        });
        const allRowCount = await this.prismaService.history_operator.count();
        const response = { rows: result, rowCount: allRowCount };
        return response;
    }
};
exports.GetOperatorHistoryService = GetOperatorHistoryService;
exports.GetOperatorHistoryService = GetOperatorHistoryService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], GetOperatorHistoryService);
//# sourceMappingURL=get-operator-history.service.js.map