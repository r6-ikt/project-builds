/// <reference types="multer" />
import { PrismaService } from "../../prisma/prisma.service";
import { ReturnUserByNameInterface } from "../../auth/dto/returnUserByName.Interface";
import { Response } from "express";
export declare class UserService {
    private prisma;
    constructor(prisma: PrismaService);
    getUserByName(username: string): Promise<ReturnUserByNameInterface>;
    getImage(res: Response, id: string): Promise<void>;
    uploadImage(file: Express.Multer.File, id: string, res: Response): Promise<void>;
    deleteImages(userId: number): Promise<void>;
}
