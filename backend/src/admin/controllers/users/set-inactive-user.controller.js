"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SetInactiveUserController = void 0;
const common_1 = require("@nestjs/common");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const set_inactive_user_service_1 = require("../../services/users/set-inactive-user.service");
const return_user_dto_1 = require("../../dto/return-user.dto");
const swagger_1 = require("@nestjs/swagger");
let SetInactiveUserController = class SetInactiveUserController {
    constructor(setInactiveUserService) {
        this.setInactiveUserService = setInactiveUserService;
    }
    setInactiveUser(id) {
        return this.setInactiveUserService.setInactiveUser(Number(id));
    }
};
exports.SetInactiveUserController = SetInactiveUserController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: return_user_dto_1.returnUserDto, description: "Inaktívvá tesz egy User-t, azonosító alapján!" }),
    (0, swagger_1.ApiParam)({ name: 'id', type: 'number', required: true }),
    (0, common_1.Post)('/inactive-user/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], SetInactiveUserController.prototype, "setInactiveUser", null);
exports.SetInactiveUserController = SetInactiveUserController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Users'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [set_inactive_user_service_1.SetInactiveUserService])
], SetInactiveUserController);
//# sourceMappingURL=set-inactive-user.controller.js.map