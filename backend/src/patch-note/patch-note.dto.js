"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchNoteDto = void 0;
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const swagger_1 = require("@nestjs/swagger");
class PatchNoteDto {
}
exports.PatchNoteDto = PatchNoteDto;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator azonosítója!", example: 1, type: Number }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_transformer_1.Type)(() => Number),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PatchNoteDto.prototype, "operator_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az Admin azonosítója!", example: 1, type: Number }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_transformer_1.Type)(() => Number),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], PatchNoteDto.prototype, "admin_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator frissített tulajdonsága!", example: 'hp', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_transformer_1.Type)(() => String),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PatchNoteDto.prototype, "updated_field", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A frissítés előtti érték!", example: '2', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_transformer_1.Type)(() => String),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PatchNoteDto.prototype, "previous_value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A frissítés utáni érték!", example: '3', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_transformer_1.Type)(() => String),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PatchNoteDto.prototype, "new_value", void 0);
//# sourceMappingURL=patch-note.dto.js.map