import { UserRegisterDto } from "../../auth/dto/userRegister.dto";
import { UpdateUserDataService } from "../services/update-user-datas.service";
import { returnUserDto } from "../../admin/dto/return-user.dto";
import { UserUpdateWithPassword } from "../../auth/dto/UserUpdateWithPassword";
export declare class UpdateUserDataController {
    private readonly updateUserDataService;
    constructor(updateUserDataService: UpdateUserDataService);
    updateUserWithoutPassword(updateUser: UserRegisterDto, id: string): Promise<returnUserDto>;
    updateUserWithPassword(updateUser: UserUpdateWithPassword, id: string): Promise<returnUserDto>;
}
