"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchNoteService = exports.PatchNoteResponseProps = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const return_patch_note_dto_1 = require("./return-patch-note.dto");
const swagger_1 = require("@nestjs/swagger");
const close_issues_service_1 = require("../admin/services/issues/close-issues.service");
class PatchNoteResponseProps {
}
exports.PatchNoteResponseProps = PatchNoteResponseProps;
__decorate([
    (0, swagger_1.ApiProperty)({ type: return_patch_note_dto_1.ReturnPatchNoteDto, required: true, isArray: true, description: "A patch_note tábla tartalma egy listában!", example: { patch_date: "2024-04-17", patch_note: "ace's test changed from test to afterTest by admin.", patch_id: 1 } }),
    __metadata("design:type", Array)
], PatchNoteResponseProps.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az adatbázis sorainak száma!", example: 12, type: Number }),
    __metadata("design:type", Number)
], PatchNoteResponseProps.prototype, "rowCount", void 0);
let PatchNoteService = class PatchNoteService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async getAllOrderedByDate(role, page, size, sort) {
        let patch_notes = [];
        if (page === null || size === null || sort === null) {
            const patch_notes_local = await this.prismaService.patch_note.findMany({
                orderBy: {
                    patch_date: 'desc',
                },
            });
            for (const patchNote of patch_notes_local) {
                patch_notes.push(patchNote);
            }
        }
        else {
            const sortParams = sort.split(',');
            const field = sortParams[0];
            const sortMode = sortParams[1];
            const perPage = size;
            const offset = (page) * perPage;
            let orderBy = {};
            if (sort) {
                orderBy = {
                    [field]: sortMode
                };
            }
            const result = await this.prismaService.patch_note.findMany({
                take: perPage,
                skip: offset,
                orderBy,
            });
            for (const resultElement of result) {
                patch_notes.push(resultElement);
            }
        }
        const returnPatchNotes = [];
        for (const patchNote of patch_notes) {
            const operator = await this.prismaService.operators.findFirst({
                where: {
                    operator_id: patchNote.operator_id,
                },
                select: {
                    name: true,
                },
            });
            let note = '';
            if (role === 'guest') {
                note = `${operator.name}'s ${patchNote.updated_field} changed from ${patchNote.previous_value} to ${patchNote.new_value}.`;
            }
            else if (role === 'admin') {
                const admin = await this.prismaService.users.findFirst({
                    select: {
                        username: true,
                    },
                    where: {
                        user_id: patchNote.admin_id,
                    },
                });
                note = `${operator.name}'s ${patchNote.updated_field} changed from ${patchNote.previous_value} to ${patchNote.new_value} by ${admin.username}.`;
            }
            else {
                throw new common_1.BadRequestException('Invalid end-point called!');
            }
            const returnPatchNote = { patch_date: (0, close_issues_service_1.formatDateWithOutTime)(patchNote.patch_date), patch_note: note, patch_id: patchNote.patch_id };
            returnPatchNotes.push(returnPatchNote);
        }
        const allRowCount = await this.prismaService.patch_note.count();
        const responseObj = { rows: returnPatchNotes, rowCount: allRowCount };
        return responseObj;
    }
};
exports.PatchNoteService = PatchNoteService;
exports.PatchNoteService = PatchNoteService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], PatchNoteService);
//# sourceMappingURL=patch-note.service.js.map