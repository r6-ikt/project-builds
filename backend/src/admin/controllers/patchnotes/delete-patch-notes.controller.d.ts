import { DeletePatchNotesService } from "../../services/patchnotes/delete-patch-notes/delete-patch-notes.service";
export declare class DeletePatchNotesController {
    private readonly DeletePatchNoteService;
    constructor(DeletePatchNoteService: DeletePatchNotesService);
    deletePatchNotes(id: string): Promise<void>;
}
