"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRoleForUserController = void 0;
const common_1 = require("@nestjs/common");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const admin_role_for_user_service_1 = require("../../services/users/admin-role-for-user.service");
const return_user_dto_1 = require("../../dto/return-user.dto");
const swagger_1 = require("@nestjs/swagger");
let AdminRoleForUserController = class AdminRoleForUserController {
    constructor(createAdminFromUserService) {
        this.createAdminFromUserService = createAdminFromUserService;
    }
    changeToAdmin(id) {
        return this.createAdminFromUserService.changeToAdmin(Number(id));
    }
};
exports.AdminRoleForUserController = AdminRoleForUserController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: return_user_dto_1.returnUserDto, description: "Admin jogosultásgot ad egy User-nek, majd visszaadja azt a User-t!" }),
    (0, swagger_1.ApiParam)({ name: 'id', type: 'number', required: true }),
    (0, common_1.Post)('/change-admin/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AdminRoleForUserController.prototype, "changeToAdmin", null);
exports.AdminRoleForUserController = AdminRoleForUserController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Users'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [admin_role_for_user_service_1.AdminRoleForUserService])
], AdminRoleForUserController);
//# sourceMappingURL=admin-role-for-user.controller.js.map