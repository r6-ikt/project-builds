"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetPatchNotesController = void 0;
const common_1 = require("@nestjs/common");
const patch_note_service_1 = require("../../../patch-note/patch-note.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const swagger_1 = require("@nestjs/swagger");
let GetPatchNotesController = class GetPatchNotesController {
    constructor(patchNoteService) {
        this.patchNoteService = patchNoteService;
    }
    getAllOrderedByDate(page, size, sort) {
        const role = 'admin';
        return this.patchNoteService.getAllOrderedByDate(role, Number(page), Number(size), sort);
    }
};
exports.GetPatchNotesController = GetPatchNotesController;
__decorate([
    (0, swagger_1.ApiQuery)({ name: 'size', required: false, type: Number }),
    (0, swagger_1.ApiQuery)({ name: 'sort', required: false, type: String }),
    (0, swagger_1.ApiResponse)({ status: 200, description: 'Visszadja a patch_note tábla tartalmát oldalanként!', type: patch_note_service_1.PatchNoteResponseProps }),
    (0, common_1.Get)('/patch-notes/:param'),
    __param(0, (0, common_1.Param)('param')),
    __param(1, (0, common_1.Query)('size')),
    __param(2, (0, common_1.Query)('sort')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String]),
    __metadata("design:returntype", Promise)
], GetPatchNotesController.prototype, "getAllOrderedByDate", null);
exports.GetPatchNotesController = GetPatchNotesController = __decorate([
    (0, swagger_1.ApiTags)('Admin', 'patch-notes'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiBearerAuth)('access-token'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [patch_note_service_1.PatchNoteService])
], GetPatchNotesController);
//# sourceMappingURL=get-patch-notes.controller.js.map