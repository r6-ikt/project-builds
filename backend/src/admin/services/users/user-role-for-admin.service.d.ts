import { PrismaService } from "../../../prisma/prisma.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class UserRoleForAdminService {
    private prismaService;
    constructor(prismaService: PrismaService);
    changeToUser(id: number): Promise<returnUserDto>;
}
