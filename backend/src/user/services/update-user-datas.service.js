"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUserDataService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../prisma/prisma.service");
const auth_service_1 = require("../../auth/services/auth.service");
const bcrypt_1 = require("bcrypt");
let UpdateUserDataService = class UpdateUserDataService {
    constructor(prisma, authService) {
        this.prisma = prisma;
        this.authService = authService;
    }
    async updateUserWithoutPassword(updateUser, id) {
        if (await this.isUserExists(id)) {
            if (await this.validateUser(updateUser, id)) {
                return this.prisma.users.update({
                    where: {
                        user_id: id
                    },
                    data: {
                        username: updateUser.username,
                        email: updateUser.email,
                    }
                });
            }
            else {
                throw new common_1.BadRequestException(`Wrong password!`);
            }
        }
    }
    async updateUserWithPassword(updateUser, id) {
        if (await this.isUserExists(id)) {
            if (await this.validateUser(updateUser, id)) {
                return this.prisma.users.update({
                    where: {
                        user_id: id
                    },
                    data: {
                        username: updateUser.username,
                        email: updateUser.email,
                        password: this.authService.hash(updateUser.newPassword),
                    }
                });
            }
            else {
                throw new common_1.BadRequestException(`Wrong password!`);
            }
        }
    }
    async validateUser(updateUser, id) {
        const result = await this.prisma.users.findFirst({
            where: {
                user_id: id
            },
            select: {
                password: true
            }
        });
        return await (0, bcrypt_1.compare)(updateUser.password, result.password);
    }
    async isUserExists(id) {
        const count = await this.prisma.users.count({
            where: {
                user_id: id,
            },
        });
        if (count === 0) {
            throw new common_1.NotFoundException('No such user!');
        }
        return true;
    }
};
exports.UpdateUserDataService = UpdateUserDataService;
exports.UpdateUserDataService = UpdateUserDataService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService, auth_service_1.AuthService])
], UpdateUserDataService);
//# sourceMappingURL=update-user-datas.service.js.map