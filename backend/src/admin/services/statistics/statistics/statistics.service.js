"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatisticsService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../../prisma/prisma.service");
let StatisticsService = class StatisticsService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async getHistoryRegisterStatistics(dates, limit, today) {
        return await Promise.all(dates.map(async (date) => {
            const count = await this.prismaService.users.count({
                where: {
                    create_at: {
                        equals: date,
                        gte: limit,
                        lt: today
                    }
                }
            });
            return { create_date: date, count };
        }));
    }
    async getHistoryUserStatistics(limit, today, dates) {
        return await Promise.all(dates.map(async (date) => {
            const count = await this.prismaService.history_user.count({
                where: {
                    history_date: {
                        equals: date,
                        gte: limit,
                        lt: today
                    }
                }
            });
            return { history_date: date, count };
        }));
    }
    async getLastSevenDays() {
        const currentDate = new Date();
        const limit = new Date(currentDate.getTime() - 7 * 24 * 60 * 60 * 1000);
        const dates = [];
        for (let i = 0; i < 7; i++) {
            const date = new Date(limit.getTime() + i * 24 * 60 * 60 * 1000);
            dates.push(date);
        }
        const historyUserStatistics = await this.getHistoryUserStatistics(limit, currentDate, dates);
        const historyRegisterStatistics = await this.getHistoryRegisterStatistics(dates, limit, currentDate);
        return {
            historyCorrectGuesses: historyUserStatistics,
            historyRegisterStatistics: historyRegisterStatistics
        };
    }
};
exports.StatisticsService = StatisticsService;
exports.StatisticsService = StatisticsService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], StatisticsService);
//# sourceMappingURL=statistics.service.js.map