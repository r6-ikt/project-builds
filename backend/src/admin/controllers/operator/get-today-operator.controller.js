"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetTodayOperatorController = void 0;
const common_1 = require("@nestjs/common");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const get_today_operator_service_1 = require("../../services/operator/get-today-operator.service");
const operatorDto_1 = require("../../../operator/dto/operatorDto");
const swagger_1 = require("@nestjs/swagger");
let GetTodayOperatorController = class GetTodayOperatorController {
    constructor(getTodayOperatorService) {
        this.getTodayOperatorService = getTodayOperatorService;
    }
    getTodayOperator() {
        return this.getTodayOperatorService.getTodayOperator();
    }
};
exports.GetTodayOperatorController = GetTodayOperatorController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: operatorDto_1.OperatorDto, description: "Visszaadja a mai operator-t!" }),
    (0, common_1.Get)('/todayOperator'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", operatorDto_1.OperatorDto)
], GetTodayOperatorController.prototype, "getTodayOperator", null);
exports.GetTodayOperatorController = GetTodayOperatorController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Operator'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [get_today_operator_service_1.GetTodayOperatorService])
], GetTodayOperatorController);
//# sourceMappingURL=get-today-operator.controller.js.map