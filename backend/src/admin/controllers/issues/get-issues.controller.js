"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetIssuesController = void 0;
const common_1 = require("@nestjs/common");
const get_issues_service_1 = require("../../services/issues/get-issues.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const return_issue_dto_1 = require("../../dto/return-issue.dto");
const swagger_1 = require("@nestjs/swagger");
let GetIssuesController = class GetIssuesController {
    constructor(getIssuesService) {
        this.getIssuesService = getIssuesService;
    }
    async getIssues(page, size, sort) {
        return this.getIssuesService.getIssues(size, page, sort);
    }
    async getOneIssue(id) {
        return this.getIssuesService.getOneIssue(Number(id));
    }
    async getRecentIssues(recentX) {
        return this.getIssuesService.getRecentIssues(Number(recentX));
    }
};
exports.GetIssuesController = GetIssuesController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: get_issues_service_1.IssueResponseProps, description: "Oldalanként adja át az összes Issue-t!" }),
    (0, swagger_1.ApiParam)({ name: 'page', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'size', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'sort', type: 'string', required: true }),
    (0, common_1.Get)('/get-issues/page/:page'),
    __param(0, (0, common_1.Param)('page')),
    __param(1, (0, common_1.Query)('size')),
    __param(2, (0, common_1.Query)('sort')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String]),
    __metadata("design:returntype", Promise)
], GetIssuesController.prototype, "getIssues", null);
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: return_issue_dto_1.ReturnIssueDto, description: "Visszaad egy Issue-t!" }),
    (0, swagger_1.ApiParam)({ name: 'id', type: 'number', required: true }),
    (0, common_1.Get)('/get-issues/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], GetIssuesController.prototype, "getOneIssue", null);
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: return_issue_dto_1.ReturnIssueDto, description: "Visszadja az utolsó x darab Issue-t!", isArray: true }),
    (0, swagger_1.ApiParam)({ name: 'recentX', type: 'number', required: true }),
    (0, common_1.Get)('get-issues/recent/:x'),
    __param(0, (0, common_1.Param)('x')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], GetIssuesController.prototype, "getRecentIssues", null);
exports.GetIssuesController = GetIssuesController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Issues'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [get_issues_service_1.GetIssuesService])
], GetIssuesController);
//# sourceMappingURL=get-issues.controller.js.map