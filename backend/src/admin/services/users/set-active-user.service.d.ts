import { PrismaService } from "../../../prisma/prisma.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class SetActiveUserService {
    private prismaService;
    constructor(prismaService: PrismaService);
    setActiveUser(id: number): Promise<returnUserDto>;
}
