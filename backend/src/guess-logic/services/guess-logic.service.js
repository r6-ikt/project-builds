"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GuessLogicService = exports.guessedOperator = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../prisma/prisma.service");
const generateOperator_1 = require("../../task-scheluding/generateOperator");
const close_issues_service_1 = require("../../admin/services/issues/close-issues.service");
exports.guessedOperator = null;
const maximumPointsPerDay = 100;
const losePointsPerGuess = 5;
const correctGuess = {
    hp: true,
    side: true,
    speed: true,
    release_date: 1,
    nationality: true,
    weapon1: true,
    weapon2: true,
    squad: true,
};
let GuessLogicService = class GuessLogicService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async findGuessedOperator(guessedName, request, role) {
        if (role === 'guest') {
            const guestToken = request.cookies['guest_token'];
            try {
                await this.prismaService.guest_user.findMany({
                    where: {
                        guest_token: guestToken,
                    },
                });
            }
            catch (e) {
                throw new common_1.BadRequestException('Invalid guest Token!');
            }
            await this.getOperator(guessedName);
            await this.guestGuessValidation(guestToken);
            const compare = compareOperator(exports.guessedOperator, generateOperator_1.todayOperator);
            return { compare: compare, guessed: exports.guessedOperator };
        }
        else if (role === 'user') {
            const requestUser = request.user;
            await this.getOperator(guessedName);
            await this.userGuessValidation(requestUser);
            const compare = compareOperator(exports.guessedOperator, generateOperator_1.todayOperator);
            if (this.compareGuesses(compare, correctGuess)) {
                await this.clientGuessedCorrectly(requestUser, request);
            }
            return { compare: compare, guessed: exports.guessedOperator };
        }
    }
    async guestGuessValidation(guestToken) {
        if (exports.guessedOperator === undefined || null) {
            throw new common_1.NotFoundException('No such operator!');
        }
        try {
            await this.prismaService.guest_guesses.create({
                data: {
                    guest_token: guestToken,
                    operator_id: exports.guessedOperator.operator_id
                }
            });
        }
        catch (e) {
            throw new common_1.BadRequestException("You already guessed this operator!");
        }
    }
    async userGuessValidation(requestUser) {
        if (exports.guessedOperator === undefined || null) {
            throw new common_1.NotFoundException('No such operator!');
        }
        try {
            await this.prismaService.guesses.create({
                data: {
                    user_id: requestUser.user_id,
                    operator_id: exports.guessedOperator.operator_id
                }
            });
        }
        catch (e) {
            throw new common_1.BadRequestException("You already guessed this operator!");
        }
    }
    async getOperator(operatorName) {
        const guessed = await this.prismaService.operators.findMany({
            where: {
                name: {
                    equals: operatorName,
                },
                active: {
                    equals: true,
                }
            }
        });
        exports.guessedOperator = guessed[0];
    }
    async clientGuessedCorrectly(requestUser, request) {
        const guesses = await this.prismaService.guesses.findMany({
            select: {
                operator_id: true,
            },
            where: {
                user_id: {
                    equals: request.user.user_id,
                },
            }
        });
        let earned_points = maximumPointsPerDay - ((guesses.length - 1) * losePointsPerGuess);
        if (earned_points < 0) {
            earned_points = 0;
        }
        const statsBeforeTheCorrectGuess = await this.prismaService.users.findUnique({
            where: {
                user_id: requestUser.user_id
            },
            select: {
                point: true,
                streak: true,
            },
        });
        await this.prismaService.users.update({
            where: {
                user_id: requestUser.user_id,
            },
            data: {
                point: statsBeforeTheCorrectGuess.point + earned_points,
                streak: statsBeforeTheCorrectGuess.streak + 1,
            },
        });
        return this.prismaService.history_user.create({
            data: {
                user_id: requestUser.user_id,
                history_date: (0, close_issues_service_1.formatDate)(new Date(Date.now())),
                earned_points: earned_points,
            }
        });
    }
    compareGuesses(obj1, obj2) {
        const obj1Keys = Object.keys(obj1);
        const obj2Keys = Object.keys(obj2);
        if (obj1Keys.length !== obj2Keys.length) {
            return false;
        }
        for (const key of obj1Keys) {
            if (obj1[key] !== obj2[key]) {
                return false;
            }
        }
        return true;
    }
    async getGuesses(user_id) {
        const returnGuesses = [];
        const guesses = await this.prismaService.guesses.findMany({
            select: {
                operator_id: true,
            },
            where: {
                user_id: {
                    equals: user_id
                }
            }
        });
        if (guesses.length != 0) {
            for (const guess of guesses) {
                const getOperator = await this.prismaService.operators.findMany({
                    where: {
                        operator_id: guess.operator_id,
                        active: true,
                    }
                });
                const differences = compareOperator(getOperator[0], generateOperator_1.todayOperator);
                const response = { guessed: getOperator[0], compare: differences };
                returnGuesses.unshift(response);
            }
            if (returnGuesses.length != 0) {
                return returnGuesses;
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    }
};
exports.GuessLogicService = GuessLogicService;
exports.GuessLogicService = GuessLogicService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], GuessLogicService);
function compareOperator(guessedOperator, todayOperator) {
    const differences = {
        side: false,
        hp: false,
        speed: false,
        release_date: 0,
        nationality: false,
        weapon1: false,
        weapon2: false,
        squad: false,
    };
    if (guessedOperator.side === todayOperator.side) {
        differences.side = true;
    }
    if (guessedOperator.hp === todayOperator.hp) {
        differences.hp = true;
    }
    if (guessedOperator.speed === todayOperator.speed) {
        differences.speed = true;
    }
    if (guessedOperator.release_date === todayOperator.release_date) {
        differences.release_date = 1;
    }
    else if (guessedOperator.release_date > todayOperator.release_date) {
        differences.release_date = 0;
    }
    else {
        differences.release_date = 2;
    }
    if (guessedOperator.nationality === todayOperator.nationality) {
        differences.nationality = true;
    }
    if (guessedOperator.weapon1 === todayOperator.weapon1) {
        differences.weapon1 = true;
    }
    if (guessedOperator.weapon2 === todayOperator.weapon2) {
        differences.weapon2 = true;
    }
    if (guessedOperator.squad === todayOperator.squad) {
        differences.squad = true;
    }
    return differences;
}
//# sourceMappingURL=guess-logic.service.js.map