export interface ReturnUserHistoryInterface {
    id: number;
    user_id: number;
    history_date: Date;
    earned_points: number;
}
