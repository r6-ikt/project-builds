"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePatchNotesDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class UpdatePatchNotesDto {
}
exports.UpdatePatchNotesDto = UpdatePatchNotesDto;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A patch-note azonosítója!", example: 1, type: Number }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], UpdatePatchNotesDto.prototype, "patch_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az Operator azonosítója!", example: 1, type: Number }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], UpdatePatchNotesDto.prototype, "operator_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az Admin azonosítója!", example: 1, type: Number }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsNumber)(),
    __metadata("design:type", Number)
], UpdatePatchNotesDto.prototype, "admin_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A frissített tulajdonság neve!", example: 'hp', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdatePatchNotesDto.prototype, "updated_field", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A frissítés előtti érték!", example: '2', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdatePatchNotesDto.prototype, "previous_value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A frissítés utáni érték!", example: '3', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdatePatchNotesDto.prototype, "new_value", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A patch-note dátuma!", example: '2024-04-16', type: String }),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], UpdatePatchNotesDto.prototype, "patch_date", void 0);
//# sourceMappingURL=update-patch-notes.dto.js.map