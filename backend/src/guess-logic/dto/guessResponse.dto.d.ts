import { OperatorDto } from "../../operator/dto/operatorDto";
import { OperatorCompareDto } from "../../operator/dto/operator-compareDto";
export interface GuessResponseDto {
    guessed: OperatorDto;
    compare: OperatorCompareDto;
}
