import { DeleteIssueService } from "../../services/issues/delete-issues.service";
export declare class DeleteIssueController {
    private readonly deleteIssueService;
    constructor(deleteIssueService: DeleteIssueService);
    deleteAllIssue(): Promise<void>;
    deleteIssue(id: string): Promise<void>;
}
