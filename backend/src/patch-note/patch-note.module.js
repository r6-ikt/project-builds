"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchNoteModule = void 0;
const common_1 = require("@nestjs/common");
const patch_note_service_1 = require("./patch-note.service");
const patch_note_controller_1 = require("./patch-note.controller");
const prisma_service_1 = require("../prisma/prisma.service");
let PatchNoteModule = class PatchNoteModule {
};
exports.PatchNoteModule = PatchNoteModule;
exports.PatchNoteModule = PatchNoteModule = __decorate([
    (0, common_1.Module)({
        controllers: [patch_note_controller_1.PatchNoteController],
        providers: [patch_note_service_1.PatchNoteService, prisma_service_1.PrismaService],
        imports: [],
        exports: [patch_note_service_1.PatchNoteService],
    })
], PatchNoteModule);
//# sourceMappingURL=patch-note.module.js.map