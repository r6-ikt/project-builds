"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const issuehelper_module_1 = require("./issuehelper/issuehelper.module");
const user_module_1 = require("./user/modules/user.module");
const operator_module_1 = require("./operator/operator.module");
const prisma_service_1 = require("./prisma/prisma.service");
const schedule_1 = require("@nestjs/schedule");
const admin_module_1 = require("./admin/admin.module");
const guess_logic_module_1 = require("./guess-logic/guess-logic.module");
const auth_module_1 = require("./auth/modules/auth.module");
const core_1 = require("@nestjs/core");
const jwt_auth_guard_1 = require("./auth/guards/jwt-auth.guard");
const leaderboard_module_1 = require("./leaderboard/leaderboard.module");
const jwt_1 = require("@nestjs/jwt");
const guest_module_1 = require("./guest/guest.module");
const patch_note_module_1 = require("./patch-note/patch-note.module");
const process = require("process");
const passport_1 = require("@nestjs/passport");
const guest_strategy_1 = require("./auth/strategies/guest-strategy");
const jwt_strategy_1 = require("./auth/strategies/jwt-strategy");
const refresh_strategy_1 = require("./auth/strategies/refresh-strategy");
const local_strategy_1 = require("./auth/strategies/local-strategy");
let AppModule = class AppModule {
};
exports.AppModule = AppModule;
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            issuehelper_module_1.IssuehelperModule, user_module_1.UserModule, operator_module_1.OperatorModule, config_1.ConfigModule.forRoot(), schedule_1.ScheduleModule.forRoot(), admin_module_1.AdminModule, guess_logic_module_1.GuessLogicModule, auth_module_1.AuthModule, leaderboard_module_1.LeaderboardModule, guest_module_1.GuestModule, patch_note_module_1.PatchNoteModule,
            passport_1.PassportModule.register({ defaultStrategy: 'jwt' }),
            jwt_1.JwtModule.register({
                secret: `${process.env.jwt_secret}`,
                signOptions: { expiresIn: '1h' },
            }),
            passport_1.PassportModule.register({ defaultStrategy: 'guest' }),
            jwt_1.JwtModule.register({
                secret: `${process.env.guest_secret}`,
                signOptions: { expiresIn: '365d' },
            }),
            passport_1.PassportModule.register({ defaultStrategy: 'refresh-token' }),
            jwt_1.JwtModule.register({
                secret: `${process.env.refresh_secret}`,
                signOptions: { expiresIn: '5d' },
            }), guest_module_1.GuestModule, patch_note_module_1.PatchNoteModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, prisma_service_1.PrismaService, guest_strategy_1.GuestStrategy, jwt_strategy_1.JwtStrategy, refresh_strategy_1.RefreshStrategy, local_strategy_1.LocalStrategy, {
                provide: core_1.APP_GUARD,
                useClass: jwt_auth_guard_1.JwtAuthGuard,
            }],
    })
], AppModule);
//# sourceMappingURL=app.module.js.map