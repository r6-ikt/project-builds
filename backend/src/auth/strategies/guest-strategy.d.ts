import { Strategy } from 'passport-local';
import { AuthService } from '../services/auth.service';
import { GuestService } from "../../guest/guest.service";
declare const GuestStrategy_base: new (...args: any[]) => Strategy;
export declare class GuestStrategy extends GuestStrategy_base {
    private authService;
    private guestService;
    constructor(authService: AuthService, guestService: GuestService);
    validate(request: any): Promise<any>;
}
export {};
