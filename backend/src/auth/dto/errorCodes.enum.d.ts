export declare enum ErrorCodes {
    USERNAME_ALREADY_EXISTS = "This username is already taken!",
    EMAIL_ALREADY_EXITS = "This email is already taken!",
    INVALID_EMAIL_FORMAT = "Invalid email format",
    WRONG_PASSWORD = "The password must be at least eight characters long and contain at least one uppercase letter and one digit"
}
