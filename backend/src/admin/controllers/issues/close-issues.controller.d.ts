import { CloseIssuesService } from "../../services/issues/close-issues.service";
import { ReturnIssueDto } from "../../dto/return-issue.dto";
export declare class CloseIssuesController {
    private readonly closeIssuesService;
    constructor(closeIssuesService: CloseIssuesService);
    closeAllIssue(): Promise<void>;
    closeIssue(id: string): Promise<ReturnIssueDto>;
}
