"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IssuehelperModule = void 0;
const common_1 = require("@nestjs/common");
const send_issue_controller_1 = require("./controllers/send-issue/send.issue.controller");
const send_issue_service_1 = require("./services/send-issue/send.issue.service");
const prisma_service_1 = require("../prisma/prisma.service");
const auth_module_1 = require("../auth/modules/auth.module");
const user_module_1 = require("../user/modules/user.module");
let IssuehelperModule = class IssuehelperModule {
};
exports.IssuehelperModule = IssuehelperModule;
exports.IssuehelperModule = IssuehelperModule = __decorate([
    (0, common_1.Module)({
        providers: [send_issue_service_1.SendIssueService, prisma_service_1.PrismaService],
        controllers: [send_issue_controller_1.SendIssueController],
        imports: [auth_module_1.AuthModule, user_module_1.UserModule]
    })
], IssuehelperModule);
//# sourceMappingURL=issuehelper.module.js.map