import { PrismaService } from "../../../../prisma/prisma.service";
export declare class DeletePatchNotesService {
    private prismaService;
    constructor(prismaService: PrismaService);
    deletePatchNote(id: number): Promise<void>;
}
