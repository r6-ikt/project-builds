import { GuessLogicService } from "../services/guess-logic.service";
import { GuessResponseDto } from "../dto/guessResponse.dto";
export declare class GuessLogicController {
    private readonly guessLogicService;
    constructor(guessLogicService: GuessLogicService);
    findGuessedOperator(guessedOperator: string, req: any): Promise<GuessResponseDto>;
    getGuessesById(req: any): Promise<GuessResponseDto[]> | null;
    findGuessedOperatorGuest(request: any, operatorName: string): Promise<GuessResponseDto>;
}
