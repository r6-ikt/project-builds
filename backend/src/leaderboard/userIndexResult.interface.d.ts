export interface UserIndexResult {
    index: number;
    username: string;
}
