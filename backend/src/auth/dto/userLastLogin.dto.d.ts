export declare class UserLastLoginDto {
    username: string;
    last_login: string;
}
