import { GetTodayOperatorService } from "../../services/operator/get-today-operator.service";
import { OperatorDto } from "../../../operator/dto/operatorDto";
export declare class GetTodayOperatorController {
    private readonly getTodayOperatorService;
    constructor(getTodayOperatorService: GetTodayOperatorService);
    getTodayOperator(): OperatorDto;
}
