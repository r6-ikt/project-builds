"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Guest = exports.IS_GUEST_KEY = void 0;
const common_1 = require("@nestjs/common");
exports.IS_GUEST_KEY = 'isGuest';
const Guest = () => (0, common_1.SetMetadata)(exports.IS_GUEST_KEY, true);
exports.Guest = Guest;
//# sourceMappingURL=is-guest.decorator.js.map