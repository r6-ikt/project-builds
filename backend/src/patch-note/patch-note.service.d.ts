import { PrismaService } from "../prisma/prisma.service";
import { ReturnPatchNoteDto } from "./return-patch-note.dto";
export declare class PatchNoteResponseProps {
    rows: ReturnPatchNoteDto[];
    rowCount: number;
}
export declare class PatchNoteService {
    private prismaService;
    constructor(prismaService: PrismaService);
    getAllOrderedByDate(role: string, page: number, size: number, sort: string): Promise<PatchNoteResponseProps>;
}
