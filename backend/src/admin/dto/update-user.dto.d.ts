export declare class UpdateUserDto {
    username: string;
    email: string;
    streak: number;
    point: number;
    admin: boolean;
    create_at: string;
    last_login: string;
    active: boolean;
}
