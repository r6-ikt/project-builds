import { PrismaService } from "../../../prisma/prisma.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class AdminRoleForUserService {
    private prismaService;
    constructor(prismaService: PrismaService);
    changeToAdmin(id: number): Promise<returnUserDto>;
}
