"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateController = void 0;
const common_1 = require("@nestjs/common");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const user_update_service_1 = require("../../services/users/user-update.service");
const update_user_dto_1 = require("../../dto/update-user.dto");
const swagger_1 = require("@nestjs/swagger");
let UserUpdateController = class UserUpdateController {
    constructor(userUpdateService) {
        this.userUpdateService = userUpdateService;
    }
    updateUser(updateUser, id) {
        return this.userUpdateService.updateUser(updateUser, Number(id));
    }
};
exports.UserUpdateController = UserUpdateController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 201, description: "Frissíti a kivánt User-t azonosító alapján!" }),
    (0, swagger_1.ApiParam)({ name: 'id', type: 'number', required: true, description: "A frissíteni kivánt User azonosítója!" }),
    (0, swagger_1.ApiBody)({ type: update_user_dto_1.UpdateUserDto, description: 'A User frissítésének beviteli értéke!' }),
    (0, common_1.Post)('/update-user/:id'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_user_dto_1.UpdateUserDto, String]),
    __metadata("design:returntype", void 0)
], UserUpdateController.prototype, "updateUser", null);
exports.UserUpdateController = UserUpdateController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Users'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [user_update_service_1.UserUpdateService])
], UserUpdateController);
//# sourceMappingURL=user-update.controller.js.map