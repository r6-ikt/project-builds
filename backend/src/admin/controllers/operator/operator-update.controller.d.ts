import { OperatorUpdateService } from "../../services/operator/operator-update.service";
import { returnOperatorDto } from "../../dto/return-operator.dto";
import { createOperatorInput } from "./operator-create.controller";
import { JwtService } from "@nestjs/jwt";
export declare class OperatorUpdateController {
    private readonly operatorUpdateService;
    private jwtService;
    constructor(operatorUpdateService: OperatorUpdateService, jwtService: JwtService);
    updateOperator(updateOperator: createOperatorInput, id: string, request: any): Promise<returnOperatorDto>;
}
