"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorCreateController = void 0;
const common_1 = require("@nestjs/common");
const operator_create_service_1 = require("../../services/operator/operator-create.service");
const operatorDto_1 = require("../../../operator/dto/operatorDto");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const return_operator_dto_1 = require("../../dto/return-operator.dto");
const swagger_1 = require("@nestjs/swagger");
let OperatorCreateController = class OperatorCreateController {
    constructor(operatorCreateService) {
        this.operatorCreateService = operatorCreateService;
    }
    createOperator(createOperator) {
        return this.operatorCreateService.createOperator(createOperator);
    }
};
exports.OperatorCreateController = OperatorCreateController;
__decorate([
    (0, swagger_1.ApiBody)({ type: operatorDto_1.OperatorDto, description: 'Operator létrehozásának beviteli értéke!' }),
    (0, swagger_1.ApiResponse)({ status: 201, type: return_operator_dto_1.returnOperatorDto, description: "Sikeres létrehozás után visszadja az új Operator-t!" }),
    (0, common_1.Post)('/create-operator'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], OperatorCreateController.prototype, "createOperator", null);
exports.OperatorCreateController = OperatorCreateController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Operator'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [operator_create_service_1.OperatorCreateService])
], OperatorCreateController);
//# sourceMappingURL=operator-create.controller.js.map