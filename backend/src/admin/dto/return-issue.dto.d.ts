export declare class ReturnIssueDto {
    issue_id: Number;
    user_id: Number;
    send_date: Date;
    done_date: Date;
    complaint: String;
}
