export declare class GuestPayloadInterface {
    ip_address: string;
    user_agent: string;
    referrer?: string;
    remote_hostname?: string;
    country?: string;
    region?: string;
    city?: string;
}
