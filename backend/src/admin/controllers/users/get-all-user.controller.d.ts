import { GetAllUserService, UserResponseProps } from "../../services/users/get-all-user.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class GetAllUserController {
    private readonly getAllUserService;
    constructor(getAllUserService: GetAllUserService);
    getAllUsers(): Promise<returnUserDto[]>;
    getAllUsersByPage(page: number, size: number, sort: string): Promise<UserResponseProps>;
}
