"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorSquadEnum = void 0;
var OperatorSquadEnum;
(function (OperatorSquadEnum) {
    OperatorSquadEnum["NIGHTHAVEN"] = "nighthaven";
    OperatorSquadEnum["VIPERSTRIKE"] = "viperstrike";
    OperatorSquadEnum["REDHAMMER"] = "redhammer";
    OperatorSquadEnum["WOLFGUARD"] = "wolfguard";
    OperatorSquadEnum["GHOSTEYES"] = "ghosteyes";
})(OperatorSquadEnum || (exports.OperatorSquadEnum = OperatorSquadEnum = {}));
//# sourceMappingURL=operator-squad.enum.js.map