export declare class PatchNoteDto {
    operator_id: number;
    admin_id: number;
    updated_field: string;
    previous_value: string;
    new_value: string;
}
