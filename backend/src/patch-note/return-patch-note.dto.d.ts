export declare class ReturnPatchNoteDto {
    patch_date: string;
    patch_note: string;
    patch_id: number;
}
