import { GetIssuesService, IssueResponseProps } from "../../services/issues/get-issues.service";
import { ReturnIssueDto } from "../../dto/return-issue.dto";
export declare class GetIssuesController {
    private readonly getIssuesService;
    constructor(getIssuesService: GetIssuesService);
    getIssues(page: number, size: number, sort: string): Promise<IssueResponseProps>;
    getOneIssue(id: string): Promise<ReturnIssueDto>;
    getRecentIssues(recentX: string): Promise<ReturnIssueDto[]>;
}
