import { PrismaService } from "../../prisma/prisma.service";
import { UserRegisterDto } from "../../auth/dto/userRegister.dto";
import { AuthService } from "../../auth/services/auth.service";
import { returnUserDto } from "../../admin/dto/return-user.dto";
import { UserUpdateWithPassword } from "../../auth/dto/UserUpdateWithPassword";
export declare class UpdateUserDataService {
    private prisma;
    private authService;
    constructor(prisma: PrismaService, authService: AuthService);
    updateUserWithoutPassword(updateUser: UserRegisterDto, id: number): Promise<returnUserDto>;
    updateUserWithPassword(updateUser: UserUpdateWithPassword, id: number): Promise<returnUserDto>;
    validateUser(updateUser: UserUpdateWithPassword | UserRegisterDto, id: number): Promise<boolean>;
    isUserExists(id: number): Promise<boolean>;
}
