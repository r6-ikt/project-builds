import { PrismaService } from "../../../prisma/prisma.service";
import { returnOperatorDto } from "../../dto/return-operator.dto";
import { CreatePatchNotesService } from "../patchnotes/create-patch-notes/create-patch-notes.service";
import { createOperatorInput } from "../../controllers/operator/operator-create.controller";
export declare class OperatorUpdateService {
    private prismaService;
    private CreatePatchNotesService;
    constructor(prismaService: PrismaService, CreatePatchNotesService: CreatePatchNotesService);
    updateOperator(operatorUpdate: createOperatorInput, operator_id: number, admin_id: number): Promise<returnOperatorDto>;
}
