"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetIssuesService = exports.IssueResponseProps = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../prisma/prisma.service");
const return_issue_dto_1 = require("../../dto/return-issue.dto");
const swagger_1 = require("@nestjs/swagger");
class IssueResponseProps {
}
exports.IssueResponseProps = IssueResponseProps;
__decorate([
    (0, swagger_1.ApiProperty)({ type: return_issue_dto_1.ReturnIssueDto, required: true, isArray: true, description: "Az Issue adatai egy listában!", example: { issue_id: 1, user_id: 1, send_date: "2024-04-13", done_date: "2024-04-15", complaint: "Van valami baj a NavBar-al!" } }),
    __metadata("design:type", Array)
], IssueResponseProps.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az adatbázis sorainak száma!", example: 12, type: Number }),
    __metadata("design:type", Number)
], IssueResponseProps.prototype, "rowCount", void 0);
let GetIssuesService = class GetIssuesService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async getIssues(size, page, sort) {
        const count = await this.prismaService.issuehelper.count();
        if (count === 0) {
            throw new common_1.NotFoundException('No issues!');
        }
        const sortParams = sort.split(',');
        const field = sortParams[0];
        const sortMode = sortParams[1];
        const perPage = size;
        const offset = (page) * perPage;
        let orderBy = {};
        if (sort) {
            orderBy = {
                [field]: sortMode
            };
        }
        const result = await this.prismaService.issuehelper.findMany({
            take: perPage,
            skip: offset,
            orderBy,
        });
        const response = { rows: result, rowCount: count };
        return response;
    }
    async getOneIssue(id) {
        const issues = this.prismaService.issuehelper.findMany({
            where: {
                issue_id: {
                    equals: id,
                }
            }
        });
        return issues[0];
    }
    async getRecentIssues(recent) {
        const issues = this.prismaService.issuehelper.findMany({
            where: {
                done_date: null,
            },
            orderBy: {
                send_date: 'desc',
            },
            take: recent,
        });
        return issues;
    }
};
exports.GetIssuesService = GetIssuesService;
exports.GetIssuesService = GetIssuesService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], GetIssuesService);
//# sourceMappingURL=get-issues.service.js.map