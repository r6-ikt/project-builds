export interface ReturnUserByNameInterface {
    user_id: number;
    username: string;
    email: string;
    password: string;
    admin: boolean;
    active: boolean;
    streak: number;
    point: number;
}
