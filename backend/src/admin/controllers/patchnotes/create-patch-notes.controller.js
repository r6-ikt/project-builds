"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePatchNotesController = void 0;
const common_1 = require("@nestjs/common");
const create_patch_notes_service_1 = require("../../services/patchnotes/create-patch-notes/create-patch-notes.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const patch_note_dto_1 = require("../../../patch-note/patch-note.dto");
const swagger_1 = require("@nestjs/swagger");
let CreatePatchNotesController = class CreatePatchNotesController {
    constructor(CreatePatchNotesService) {
        this.CreatePatchNotesService = CreatePatchNotesService;
    }
    async createPatchNotes(patchNote) {
        const called = 'manual';
        return await this.CreatePatchNotesService.createPatchNotes(called, patchNote);
    }
};
exports.CreatePatchNotesController = CreatePatchNotesController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 201, type: patch_note_dto_1.PatchNoteDto, description: "Létrehoz egy új patch-note-ot!" }),
    (0, swagger_1.ApiBody)({ type: patch_note_dto_1.PatchNoteDto, description: 'A patch-note létrehozásának beviteli értéke!' }),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [patch_note_dto_1.PatchNoteDto]),
    __metadata("design:returntype", Promise)
], CreatePatchNotesController.prototype, "createPatchNotes", null);
exports.CreatePatchNotesController = CreatePatchNotesController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'PatchNote'),
    (0, common_1.Controller)('create-patch-notes'),
    __metadata("design:paramtypes", [create_patch_notes_service_1.CreatePatchNotesService])
], CreatePatchNotesController);
//# sourceMappingURL=create-patch-notes.controller.js.map