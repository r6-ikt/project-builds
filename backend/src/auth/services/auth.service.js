"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = require("bcrypt");
const user_service_1 = require("../../user/services/user.service");
const jwt_1 = require("@nestjs/jwt");
const process = require("process");
const prisma_service_1 = require("../../prisma/prisma.service");
const close_issues_service_1 = require("../../admin/services/issues/close-issues.service");
const client_1 = require("@prisma/client");
const prisma_error_enum_1 = require("prisma-error-enum");
const errorCodes_enum_1 = require("../dto/errorCodes.enum");
const jwt = require("jsonwebtoken");
const guestPayload_interface_1 = require("../dto/guestPayload.interface");
let AuthService = class AuthService {
    constructor(userService, jwtService, prismaService) {
        this.userService = userService;
        this.jwtService = jwtService;
        this.prismaService = prismaService;
    }
    async validateUSer(username, password) {
        const user = await this.userService.getUserByName(username);
        if (user && await bcrypt.compare(password, user.password)) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }
    async login(user) {
        if (user.active === false) {
            throw new common_1.ForbiddenException("This user is inactive, for further information please contact us!");
        }
        const lastLoginUser = { username: user.username, last_login: (0, close_issues_service_1.formatDate)(new Date(Date.now())) };
        await this.last_login(lastLoginUser);
        return {
            accessToken: this.jwtService.sign(user, {
                secret: `${process.env.jwt_secret}`,
                expiresIn: '1h'
            }),
            refreshToken: this.jwtService.sign(user, {
                expiresIn: '1d',
                secret: `${process.env.refresh_secret}`,
            })
        };
    }
    async last_login(user) {
        await this.prismaService.users.update({
            where: {
                username: user.username
            },
            data: {
                last_login: user.last_login
            }
        });
    }
    async refreshToken(refreshToken) {
        const decodedToken = jwt.decode(refreshToken);
        const payload = { username: decodedToken.username, email: decodedToken.email, admin: decodedToken.admin, user_id: decodedToken.user_id, active: decodedToken.active, streak: decodedToken.streak, point: decodedToken.point };
        return {
            accessToken: this.jwtService.sign(payload, {
                expiresIn: '1d',
                secret: `${process.env.jwt_secret}`,
            }),
        };
    }
    hash(password) {
        const hashedPassword = bcrypt.hashSync(password, bcrypt.genSaltSync());
        if (Boolean(bcrypt.compare(password, hashedPassword))) {
            return hashedPassword;
        }
        else {
            throw new Error("Encryption failed");
        }
    }
    async CreateUser(inputUser) {
        try {
            await this.prismaService.users.create({
                data: {
                    username: inputUser.username,
                    email: inputUser.email,
                    password: this.hash(inputUser.password)
                }
            });
            return;
        }
        catch (error) {
            if (error instanceof client_1.Prisma.PrismaClientKnownRequestError) {
                if (error.code === prisma_error_enum_1.PrismaError.UniqueConstraintViolation &&
                    error.meta.target[0] === 'username') {
                    throw new common_1.BadRequestException(errorCodes_enum_1.ErrorCodes.USERNAME_ALREADY_EXISTS);
                }
                else if (error.code === prisma_error_enum_1.PrismaError.UniqueConstraintViolation &&
                    error.meta.target[0] === 'email') {
                    throw new common_1.BadRequestException(errorCodes_enum_1.ErrorCodes.EMAIL_ALREADY_EXITS);
                }
            }
        }
    }
    ;
    async guestToken(payload, res) {
        const guestToken = this.jwtService.sign(payload, {
            expiresIn: '365d',
            secret: `${process.env.guest_secret}`,
        });
        res.cookie('guest_token', guestToken, { httpOnly: true, expires: new Date(2322, 1, 1) });
        const response = await this.prismaService.guest_user.create({
            data: {
                guest_token: guestToken,
            }
        });
        if (!response) {
            throw new Error('There was an error during creating guest!');
        }
        return response;
    }
};
exports.AuthService = AuthService;
__decorate([
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [guestPayload_interface_1.GuestPayloadInterface, Object]),
    __metadata("design:returntype", Promise)
], AuthService.prototype, "guestToken", null);
exports.AuthService = AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService, jwt_1.JwtService, prisma_service_1.PrismaService])
], AuthService);
//# sourceMappingURL=auth.service.js.map