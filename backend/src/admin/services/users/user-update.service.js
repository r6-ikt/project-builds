"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../prisma/prisma.service");
const close_issues_service_1 = require("../issues/close-issues.service");
let UserUpdateService = class UserUpdateService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async updateUser(userUpdate, user_id) {
        const count = await this.prismaService.users.count({
            where: {
                user_id: {
                    equals: user_id
                }
            }
        });
        if (count === 0) {
            throw new common_1.NotFoundException('No such user!');
        }
        const user = this.prismaService.users.update({
            where: {
                user_id: user_id,
            },
            data: {
                username: userUpdate.username,
                email: userUpdate.email,
                streak: userUpdate.streak,
                point: userUpdate.point,
                admin: userUpdate.admin,
                create_at: (0, close_issues_service_1.formatDate)(new Date(userUpdate.create_at)),
                last_login: (0, close_issues_service_1.formatDate)(new Date(userUpdate.last_login)),
                active: userUpdate.active,
            },
        });
        user[0].password = '';
        return user;
    }
};
exports.UserUpdateService = UserUpdateService;
exports.UserUpdateService = UserUpdateService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UserUpdateService);
//# sourceMappingURL=user-update.service.js.map