"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteIssueController = void 0;
const common_1 = require("@nestjs/common");
const delete_issues_service_1 = require("../../services/issues/delete-issues.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const swagger_1 = require("@nestjs/swagger");
let DeleteIssueController = class DeleteIssueController {
    constructor(deleteIssueService) {
        this.deleteIssueService = deleteIssueService;
    }
    deleteAllIssue() {
        return this.deleteIssueService.deleteAllIssue();
    }
    deleteIssue(id) {
        return this.deleteIssueService.deleteIssue(Number(id));
    }
};
exports.DeleteIssueController = DeleteIssueController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 202, description: 'Törli az összes Issue-t!' }),
    (0, common_1.Post)('/delete-issues'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DeleteIssueController.prototype, "deleteAllIssue", null);
__decorate([
    (0, swagger_1.ApiResponse)({ status: 202, description: 'Töröl egy darab Issue-t!' }),
    (0, swagger_1.ApiParam)({ name: 'id', type: 'number', required: true, example: 1, description: "Az Issue azonosítója!" }),
    (0, common_1.Post)('/delete-issues/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DeleteIssueController.prototype, "deleteIssue", null);
exports.DeleteIssueController = DeleteIssueController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)("Admin", "Issues"),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [delete_issues_service_1.DeleteIssueService])
], DeleteIssueController);
//# sourceMappingURL=delete-issues.controller.js.map