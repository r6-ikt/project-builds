"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePatchNotesService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../../prisma/prisma.service");
let CreatePatchNotesService = class CreatePatchNotesService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async createPatchNotes(called, creatingPatchNote, updatedOperator, admin_id, operatorId) {
        const errors = [];
        if (called === 'manual') {
            try {
                return await this.prismaService.patch_note.create({
                    data: {
                        operator_id: creatingPatchNote.operator_id,
                        admin_id: creatingPatchNote.admin_id,
                        updated_field: creatingPatchNote.updated_field,
                        previous_value: creatingPatchNote.previous_value,
                        new_value: creatingPatchNote.new_value,
                    },
                });
            }
            catch (e) {
                throw new Error('There was an Error during creating patch note!');
            }
        }
        else if (called === 'automatic') {
            const patchNoteProps = await this.getPatchNoteFromUpdate(updatedOperator, admin_id, operatorId);
            if (patchNoteProps === null) {
                throw new common_1.NotFoundException('No such patch note!');
            }
            for (const patchNoteProp of patchNoteProps) {
                try {
                    await this.prismaService.patch_note.create({
                        data: {
                            operator_id: patchNoteProp.operator_id,
                            admin_id: patchNoteProp.admin_id,
                            updated_field: patchNoteProp.updated_field,
                            previous_value: patchNoteProp.previous_value,
                            new_value: patchNoteProp.new_value,
                        },
                    });
                }
                catch (e) {
                    errors.push(e);
                }
            }
            if (errors.length !== 0) {
                throw new Error(errors[0].message.toString());
            }
        }
        else {
            throw new common_1.BadRequestException('Invalid end-pont called!');
        }
    }
    async getPatchNoteFromUpdate(updatedOperator, admin_id, operatorId) {
        const patchNoteProps = [];
        const previousOperator = await this.prismaService.operators.findFirst({
            where: {
                operator_id: operatorId,
            },
        });
        if (!previousOperator) {
            throw new common_1.NotFoundException('No such operator');
        }
        if (updatedOperator.side !== previousOperator.side) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "side",
                previous_value: updatedOperator.side ? 'defender' : 'attacker',
                new_value: updatedOperator.side ? 'attacker' : 'defender',
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.name !== previousOperator.name) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "name",
                previous_value: previousOperator.name,
                new_value: updatedOperator.name,
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.hp !== previousOperator.hp) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "hp",
                previous_value: previousOperator.hp.toString(),
                new_value: updatedOperator.hp.toString(),
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.speed !== previousOperator.speed) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "speed",
                previous_value: previousOperator.speed.toString(),
                new_value: updatedOperator.speed.toString(),
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.release_date !== previousOperator.release_date) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "release date",
                previous_value: previousOperator.release_date.toString(),
                new_value: updatedOperator.release_date.toString(),
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.nationality !== previousOperator.nationality) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "nationality",
                previous_value: previousOperator.nationality,
                new_value: updatedOperator.nationality,
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.weapon1 !== previousOperator.weapon1) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "weapon 1",
                previous_value: previousOperator.weapon1,
                new_value: updatedOperator.weapon1,
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.weapon2 !== previousOperator.weapon2) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "weapon 2",
                previous_value: previousOperator.weapon2,
                new_value: updatedOperator.weapon2,
            };
            patchNoteProps.push(patchNote);
        }
        if (updatedOperator.squad !== previousOperator.squad) {
            const patchNote = {
                operator_id: operatorId,
                admin_id: admin_id,
                updated_field: "squad",
                previous_value: previousOperator.squad.toString(),
                new_value: updatedOperator.squad.toString(),
            };
            patchNoteProps.push(patchNote);
        }
        if (patchNoteProps.length !== 0) {
            return patchNoteProps;
        }
        else {
            return null;
        }
    }
};
exports.CreatePatchNotesService = CreatePatchNotesService;
exports.CreatePatchNotesService = CreatePatchNotesService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], CreatePatchNotesService);
//# sourceMappingURL=create-patch-notes.service.js.map