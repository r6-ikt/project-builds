import { PrismaService } from "../../../../prisma/prisma.service";
import { PatchNoteDto } from "../../../../patch-note/patch-note.dto";
import { createOperatorInput } from "../../../controllers/operator/operator-create.controller";
export declare class CreatePatchNotesService {
    private prismaService;
    constructor(prismaService: PrismaService);
    createPatchNotes(called: string, creatingPatchNote?: PatchNoteDto, updatedOperator?: createOperatorInput, admin_id?: number, operatorId?: number): Promise<PatchNoteDto>;
    getPatchNoteFromUpdate(updatedOperator: createOperatorInput, admin_id: number, operatorId: number): Promise<PatchNoteDto[]>;
}
