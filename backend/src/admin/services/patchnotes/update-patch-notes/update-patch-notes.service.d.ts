import { PrismaService } from "../../../../prisma/prisma.service";
import { UpdatePatchNotesDto } from "./update-patch-notes.dto";
export declare class UpdatePatchNotesService {
    private prismaService;
    constructor(prismaService: PrismaService);
    updatePatchNotes(update: UpdatePatchNotesDto): Promise<void>;
}
