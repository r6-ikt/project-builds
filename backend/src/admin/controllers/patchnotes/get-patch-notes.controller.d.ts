import { PatchNoteResponseProps, PatchNoteService } from "../../../patch-note/patch-note.service";
export declare class GetPatchNotesController {
    private readonly patchNoteService;
    constructor(patchNoteService: PatchNoteService);
    getAllOrderedByDate(page: string, size: string, sort: string): Promise<PatchNoteResponseProps>;
}
