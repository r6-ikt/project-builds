"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsPassword = void 0;
const class_validator_1 = require("class-validator");
let IsPassword = class IsPassword {
    validate(password, args) {
        const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
        return regex.test(password);
    }
    defaultMessage(args) {
        return 'Password must contain at least 8 characters, one uppercase letter, one lowercase letter, and one number';
    }
};
exports.IsPassword = IsPassword;
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Boolean)
], IsPassword.prototype, "validate", null);
exports.IsPassword = IsPassword = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'isPassword', async: false })
], IsPassword);
//# sourceMappingURL=password.validator.js.map