import { UserRoleForAdminService } from "../../services/users/user-role-for-admin.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class UserRoleForAdminController {
    private readonly userRoleForAdminService;
    constructor(userRoleForAdminService: UserRoleForAdminService);
    changeToUser(id: string): Promise<returnUserDto>;
}
