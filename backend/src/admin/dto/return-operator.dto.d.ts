export declare class returnOperatorDto {
    operator_id: number;
    name: string;
    side: boolean;
    hp: number;
    speed: number;
    release_date: number;
    nationality: string;
    weapon1: string;
    weapon2: string;
    squad: string;
}
