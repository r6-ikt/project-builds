import { SetActiveUserService } from "../../services/users/set-active-user.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class SetActiveUserController {
    private readonly setActiveUserService;
    constructor(setActiveUserService: SetActiveUserService);
    setInactiveUser(id: string): Promise<returnUserDto>;
}
