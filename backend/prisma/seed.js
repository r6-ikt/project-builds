"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const prisma_service_1 = require("../src/prisma/prisma.service");
const fs = require("fs");
const process = require("process");
const user_service_1 = require("../src/user/services/user.service");
const auth_service_1 = require("../src/auth/services/auth.service");
const jwt_1 = require("@nestjs/jwt");
const prisma = new client_1.PrismaClient();
async function main() {
    const prisma = new prisma_service_1.PrismaService();
    const userService = new user_service_1.UserService(prisma);
    const jwtService = new jwt_1.JwtService();
    const authService = new auth_service_1.AuthService(userService, jwtService, prisma);
    let admin = {
        email: 'admin@gmail.com',
        username: 'admin',
        password: authService.hash('Admin123'),
        admin: true,
    };
    let user = {
        email: 'user@gmail.com',
        username: 'user',
        password: authService.hash('Userpassword123'),
    };
    await prisma.users.upsert({
        where: { email: 'admin@gmail.com' },
        update: {
            email: 'admin@gmail.com',
            username: 'admin',
            password: authService.hash('Admin123'),
            admin: true,
        },
        create: admin
    });
    await prisma.users.upsert({
        where: { email: 'user@gmail.com' },
        update: {
            email: 'user@gmail.com',
            username: 'user',
            password: authService.hash('Userpassword123'),
        },
        create: user
    });
    const operators = readOperators();
    for (const operator of operators) {
        await prisma.operators.upsert({
            where: {
                name: operator.name
            },
            update: {},
            create: operator
        });
    }
}
main()
    .then(async () => {
    await prisma.$disconnect();
})
    .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
});
function readOperators() {
    let operators = [];
    let file = fs.readFileSync(process.cwd() + '/prisma/operators.csv', 'utf8');
    file = file.replaceAll('\r', '');
    const lines = file.split('\n');
    for (let i = 0; i < lines.length; i++) {
        if (i !== 0) {
            const line = lines[i].split(';');
            operators.push({
                name: line[0].toLowerCase(),
                side: line[1] === 'Attacker',
                hp: parseInt(line[2]),
                speed: parseInt(line[3]),
                release_date: parseInt(line[4]),
                nationality: line[5].toLowerCase(),
                weapon1: line[7].toLowerCase(),
                weapon2: line[8].toLowerCase(),
                squad: line[9].toLowerCase(),
            });
        }
    }
    return operators;
}
//# sourceMappingURL=seed.js.map