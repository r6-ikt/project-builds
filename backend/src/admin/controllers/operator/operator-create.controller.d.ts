import { OperatorCreateService } from "../../services/operator/operator-create.service";
import { OperatorDto } from "../../../operator/dto/operatorDto";
import { returnOperatorDto } from "../../dto/return-operator.dto";
export type createOperatorInput = Omit<OperatorDto, 'operator_id'>;
export declare class OperatorCreateController {
    private readonly operatorCreateService;
    constructor(operatorCreateService: OperatorCreateService);
    createOperator(createOperator: createOperatorInput): Promise<returnOperatorDto>;
}
