import { PrismaService } from "../../../prisma/prisma.service";
import { ReturnIssueDto } from "../../dto/return-issue.dto";
export declare class IssueResponseProps {
    rows: ReturnIssueDto[];
    rowCount: number;
}
export declare class GetIssuesService {
    private prismaService;
    constructor(prismaService: PrismaService);
    getIssues(size: number, page: number, sort: string): Promise<IssueResponseProps>;
    getOneIssue(id: number): Promise<ReturnIssueDto>;
    getRecentIssues(recent: number): Promise<ReturnIssueDto[]>;
}
