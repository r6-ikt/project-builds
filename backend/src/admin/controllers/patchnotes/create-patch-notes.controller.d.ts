import { CreatePatchNotesService } from "../../services/patchnotes/create-patch-notes/create-patch-notes.service";
import { PatchNoteDto } from "../../../patch-note/patch-note.dto";
export declare class CreatePatchNotesController {
    private CreatePatchNotesService;
    constructor(CreatePatchNotesService: CreatePatchNotesService);
    createPatchNotes(patchNote: PatchNoteDto): Promise<PatchNoteDto>;
}
