import { PatchNoteService } from './patch-note.service';
export declare class PatchNoteController {
    private readonly patchNoteService;
    constructor(patchNoteService: PatchNoteService);
    getAllPatchNotes(): Promise<import("./patch-note.service").PatchNoteResponseProps>;
}
