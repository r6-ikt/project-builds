"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendIssueService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../prisma/prisma.service");
const schedule_1 = require("@nestjs/schedule");
let SendIssueService = class SendIssueService {
    constructor(prismaService) {
        this.prismaService = prismaService;
        this.disabledUsers = [];
    }
    async sendIssue(issue, req) {
        if (this.disabledUsers.find(user => req.user.user_id === user.user_id)) {
            throw new common_1.BadRequestException("Try Again Later");
        }
        if (issue.complaint.trim() === "" || issue.complaint.trim().length < 5) {
            throw new common_1.BadRequestException('You made an invalid issue! Please try again!');
        }
        const now = new Date();
        const hour = now.getHours();
        const minute = now.getMinutes();
        const paddedMinute = minute < 10 ? `0${minute}` : `${minute}`;
        const expireHour = (hour + 1) % 24;
        const expireMinute = paddedMinute;
        const createdTime = `${hour}:${paddedMinute}`;
        const expireTime = `${expireHour}:${expireMinute}`;
        const disable = {
            user_id: req.user.user_id,
            created_at: createdTime,
            expire_at: expireTime
        };
        this.disabledUsers.push(disable);
        return this.prismaService.issuehelper.create({
            data: {
                complaint: issue.complaint,
                user_id: req.user.user_id,
            }
        });
    }
    expireDisable() {
        const expiredUsersId = [];
        for (const user of this.disabledUsers) {
            const expireTimeParts = user.expire_at.split(":");
            const expireHour = parseInt(expireTimeParts[0], 10);
            const expireMinute = parseInt(expireTimeParts[1], 10);
            const expireDate = new Date();
            expireDate.setHours(expireHour, expireMinute, 0, 0);
            const now = new Date();
            const hasExpired = now > expireDate;
            if (hasExpired) {
                expiredUsersId.push(user.user_id);
            }
        }
        for (const id of expiredUsersId) {
            const index = this.disabledUsers.findIndex(user => user.user_id === id);
            if (!(index === Number(-1))) {
                this.disabledUsers.splice(index, 1);
            }
        }
    }
};
exports.SendIssueService = SendIssueService;
__decorate([
    (0, schedule_1.Cron)(schedule_1.CronExpression.EVERY_10_MINUTES),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], SendIssueService.prototype, "expireDisable", null);
exports.SendIssueService = SendIssueService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], SendIssueService);
//# sourceMappingURL=send.issue.service.js.map