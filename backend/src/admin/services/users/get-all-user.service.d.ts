import { PrismaService } from "../../../prisma/prisma.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class UserResponseProps {
    rows: returnUserDto[];
    rowCount: number;
}
export declare class GetAllUserService {
    private prismaService;
    constructor(prismaService: PrismaService);
    getUsers(): Promise<returnUserDto[]>;
    getAllUsers(size: number, page: number, sort: string): Promise<UserResponseProps>;
}
