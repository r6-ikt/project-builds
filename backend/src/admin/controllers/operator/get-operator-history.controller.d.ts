import { GetOperatorHistoryService, HistoryOperatorResponseProps } from "../../services/operator/get-operator-history.service";
import { historyOperatorDto } from "../../dto/history-operator.dto";
export declare class GetOperatorHistoryController {
    private readonly getOperatorHistoryService;
    constructor(getOperatorHistoryService: GetOperatorHistoryService);
    getOperatorHistory(): Promise<historyOperatorDto[]>;
    getItems(page: number, size: number, sort: string): Promise<HistoryOperatorResponseProps>;
}
