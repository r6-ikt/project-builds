import { returnOperatorDto } from "../../dto/return-operator.dto";
export declare class OperatorResponseProps {
    rows: returnOperatorDto[];
    rowCount: number;
}
export declare class GetOperatorsService {
    getAllOperators(): Promise<returnOperatorDto[]>;
    getOperators(size: number, page: number, sort: string): Promise<OperatorResponseProps>;
}
