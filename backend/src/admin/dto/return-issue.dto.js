"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnIssueDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class ReturnIssueDto {
}
exports.ReturnIssueDto = ReturnIssueDto;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az issue azonosítója!", example: 123456, type: Number }),
    __metadata("design:type", Number)
], ReturnIssueDto.prototype, "issue_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A user azonosítója!", example: 123456, type: Number }),
    __metadata("design:type", Number)
], ReturnIssueDto.prototype, "user_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az issue elküldésének ideje!", example: "2024-04-06", type: Date }),
    __metadata("design:type", Date)
], ReturnIssueDto.prototype, "send_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: false, description: "Az issue lezárásank ideje!", example: "2024-04-06", type: Date }),
    __metadata("design:type", Date)
], ReturnIssueDto.prototype, "done_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az issue leírása", example: "Csúnya a NavBar!", type: String }),
    __metadata("design:type", String)
], ReturnIssueDto.prototype, "complaint", void 0);
//# sourceMappingURL=return-issue.dto.js.map