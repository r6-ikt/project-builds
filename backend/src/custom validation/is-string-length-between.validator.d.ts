import { ValidationOptions } from 'class-validator';
export declare function IsStringLengthBetween(min: number, max: number, validationOptions?: ValidationOptions): (object: Object, propertyName: string) => void;
