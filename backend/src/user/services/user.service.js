"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../prisma/prisma.service");
const path = require("path");
const fs = require("fs");
const util_1 = require("util");
let UserService = class UserService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async getUserByName(username) {
        return this.prisma.users.findFirst({
            select: {
                username: true,
                password: true,
                email: true,
                admin: true,
                user_id: true,
                active: true,
                streak: true,
                point: true,
            },
            where: {
                username: username
            }
        });
    }
    async getImage(res, id) {
        const defaultImagePath = path.resolve(__dirname, '../../../../uploads/default.png');
        const extensionsToTry = ['.jpg', '.jpeg', '.png'];
        for (const ext of extensionsToTry) {
            const imagePath = path.resolve(__dirname, `../../../../uploads/${id}${ext}`);
            if (await fileExists(imagePath)) {
                return res.sendFile(imagePath);
            }
        }
        return res.sendFile(defaultImagePath);
    }
    async uploadImage(file, id, res) {
        const userId = +id;
        const destinationFilename = `${userId}${path.extname(file.originalname)}`;
        const destinationPath = `./uploads/${destinationFilename}`;
        await this.deleteImages(userId);
        await fs.promises.writeFile(destinationPath, file.buffer);
        return await this.getImage(res, id);
    }
    async deleteImages(userId) {
        const extensionsToTry = ['.jpg', '.jpeg', '.png'];
        for (const ext of extensionsToTry) {
            const imagePath = `./uploads/${userId}${ext}`;
            if (await fileExists(imagePath)) {
                await (0, util_1.promisify)(fs.unlink)(imagePath);
            }
        }
    }
};
exports.UserService = UserService;
__decorate([
    __param(0, (0, common_1.Res)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "getImage", null);
__decorate([
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, Object]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "uploadImage", null);
exports.UserService = UserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UserService);
async function fileExists(filePath) {
    try {
        await fs.promises.access(filePath, fs.constants.F_OK);
        return true;
    }
    catch (error) {
        return false;
    }
}
//# sourceMappingURL=user.service.js.map