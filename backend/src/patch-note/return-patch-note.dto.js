"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnPatchNoteDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class ReturnPatchNoteDto {
}
exports.ReturnPatchNoteDto = ReturnPatchNoteDto;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A patch-note dátuma!", example: "2024-04-17", type: String }),
    __metadata("design:type", String)
], ReturnPatchNoteDto.prototype, "patch_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A patch-note tartalma!", example: "ace's test changed from test to afterTest by admin.", type: String }),
    __metadata("design:type", String)
], ReturnPatchNoteDto.prototype, "patch_note", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A patch-note azonosítója!", example: 1, type: Number }),
    __metadata("design:type", Number)
], ReturnPatchNoteDto.prototype, "patch_id", void 0);
//# sourceMappingURL=return-patch-note.dto.js.map