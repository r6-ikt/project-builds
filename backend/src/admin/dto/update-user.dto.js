"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateUserDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class UpdateUserDto {
}
exports.UpdateUserDto = UpdateUserDto;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User felhasználóneve!", example: "user", type: String }),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "username", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User e-mail címe!", example: "user@user.com", type: String }),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "email", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User hány egymást követő napja játszik!", example: 1, type: Number }),
    __metadata("design:type", Number)
], UpdateUserDto.prototype, "streak", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User pontszáma!", example: 1, type: Number }),
    __metadata("design:type", Number)
], UpdateUserDto.prototype, "point", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User Admin-e!", example: false, type: Boolean }),
    __metadata("design:type", Boolean)
], UpdateUserDto.prototype, "admin", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User regisztrálásának dátuma!", example: "2024-06-01", type: String }),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "create_at", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User legutóbbi belépésének dátuma!", example: "2024-06-01", type: String }),
    __metadata("design:type", String)
], UpdateUserDto.prototype, "last_login", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "A User aktivitása!", example: true, type: Boolean }),
    __metadata("design:type", Boolean)
], UpdateUserDto.prototype, "active", void 0);
//# sourceMappingURL=update-user.dto.js.map