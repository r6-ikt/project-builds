export declare class returnUserDto {
    user_id: number;
    username: string;
    email: string;
    streak: number;
    point: number;
    admin: boolean;
    create_at: Date;
    last_login: Date;
    active: boolean;
}
