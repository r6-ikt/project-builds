"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.todayOperator = exports.getOperators = exports.TasksService = void 0;
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const client_1 = require("@prisma/client");
const close_issues_service_1 = require("../admin/services/issues/close-issues.service");
const prisma = new client_1.PrismaClient();
let todayOperator = null;
exports.todayOperator = todayOperator;
let TasksService = class TasksService {
    constructor() {
        this.generateOperatorOnStartUp();
    }
    async generateOperatorOnStartUp() {
        const todayOp = await generateOperator();
        console.log(todayOp);
        exports.todayOperator = todayOperator = todayOp;
        await pushOperatorToHistory_Operator(todayOp);
        await deleteAllElementInGuessesTable();
        await deleteAllElementInGuestGuessesTable();
    }
    async generateOperatorEveryDay() {
        const todayOp = await generateOperator();
        console.log(todayOp);
        exports.todayOperator = todayOperator = todayOp;
        await pushOperatorToHistory_Operator(todayOp);
        await pushOperatorToHistory_Operator(todayOp);
        await zeroStreak();
    }
};
exports.TasksService = TasksService;
__decorate([
    (0, schedule_1.Cron)(schedule_1.CronExpression.EVERY_DAY_AT_MIDNIGHT),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TasksService.prototype, "generateOperatorEveryDay", null);
exports.TasksService = TasksService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], TasksService);
async function pushOperatorToHistory_Operator(operator) {
    const operatorToPush = { operator_id: operator.operator_id, history_date: (0, close_issues_service_1.formatDate)(new Date(Date.now())) };
    return prisma.history_operator.create({
        data: operatorToPush,
    });
}
async function generateOperator() {
    return await getRandomOperator(await getOperators());
}
async function deleteAllElementInGuessesTable() {
    return prisma.guesses.deleteMany();
}
async function deleteAllElementInGuestGuessesTable() {
    return prisma.guest_guesses.deleteMany();
}
async function getOperators() {
    const operatorsFromDB = await prisma.operators.findMany({
        where: {
            active: true
        }
    });
    return operatorsFromDB.map(operator => {
        return {
            ...operator
        };
    });
}
exports.getOperators = getOperators;
async function getRandomOperator(arr) {
    const shuffled = [...arr].sort(() => 0.5 - Math.random());
    return shuffled[0];
}
async function zeroStreak() {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    const guessedYesterDay = await prisma.history_user.findMany({
        where: {
            history_date: yesterday,
        },
        select: {
            user_id: true,
        },
    });
    const users = await prisma.users.findMany({
        select: {
            user_id: true,
        },
        where: {
            active: true,
        },
    });
    const guessId = [];
    for (const guess of guessedYesterDay) {
        guessId.push(guess.user_id);
    }
    for (const user of users) {
        if (!(guessId.includes(user.user_id, 0))) {
            await prisma.users.update({
                where: {
                    user_id: user.user_id,
                },
                data: {
                    streak: 0,
                },
            });
        }
    }
}
//# sourceMappingURL=generateOperator.js.map