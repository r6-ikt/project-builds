"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetAllUserService = exports.UserResponseProps = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../../prisma/prisma.service");
const return_user_dto_1 = require("../../dto/return-user.dto");
const swagger_1 = require("@nestjs/swagger");
class UserResponseProps {
}
exports.UserResponseProps = UserResponseProps;
__decorate([
    (0, swagger_1.ApiProperty)({ type: return_user_dto_1.returnUserDto, required: true, isArray: true, description: "A users tábla tartalma egy listában!", example: { user_id: 1, username: 'admin', email: 'admin@admin.com', streak: 2, point: 320, admin: true, create_at: '2024-01-28', last_login: '2024-05-01', active: true } }),
    __metadata("design:type", Array)
], UserResponseProps.prototype, "rows", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az adatbázis sorainak száma!", example: 12, type: Number }),
    __metadata("design:type", Number)
], UserResponseProps.prototype, "rowCount", void 0);
let GetAllUserService = class GetAllUserService {
    constructor(prismaService) {
        this.prismaService = prismaService;
    }
    async getUsers() {
        try {
            return this.prismaService.users.findMany({
                select: {
                    user_id: true,
                    username: true,
                    email: true,
                    streak: true,
                    point: true,
                    admin: true,
                    create_at: true,
                    last_login: true,
                    active: true,
                    password: false
                }
            });
        }
        catch (e) {
            throw new common_1.NotFoundException('Failed to load users!');
        }
    }
    async getAllUsers(size, page, sort) {
        const sortParams = sort.split(',');
        const field = sortParams[0];
        const sortMode = sortParams[1];
        const perPage = size;
        const offset = (page) * perPage;
        let orderBy = {};
        if (sort) {
            orderBy = {
                [field]: sortMode
            };
        }
        const result = await this.prismaService.users.findMany({
            select: {
                user_id: true,
                username: true,
                email: true,
                streak: true,
                point: true,
                admin: true,
                create_at: true,
                last_login: true,
                active: true,
            },
            take: perPage,
            skip: offset,
            orderBy,
        });
        const allRowCount = await this.prismaService.users.count();
        const response = { rows: result, rowCount: allRowCount };
        return response;
    }
};
exports.GetAllUserService = GetAllUserService;
exports.GetAllUserService = GetAllUserService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], GetAllUserService);
//# sourceMappingURL=get-all-user.service.js.map