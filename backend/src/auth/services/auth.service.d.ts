import { UserService } from "../../user/services/user.service";
import { JwtService } from "@nestjs/jwt";
import { UserLastLoginDto } from "../dto/userLastLogin.dto";
import { PrismaService } from "../../prisma/prisma.service";
import { ReturnUserByNameInterface } from "../dto/returnUserByName.Interface";
import { ReturnTokenInterface } from "../dto/returnToken.interface";
import { UserRegisterDto } from "../dto/userRegister.dto";
import { GuestPayloadInterface } from "../dto/guestPayload.interface";
import { Response } from "express";
export type UserDataWithoutPassword = Omit<ReturnUserByNameInterface, "password">;
export declare class AuthService {
    private readonly userService;
    private jwtService;
    private prismaService;
    constructor(userService: UserService, jwtService: JwtService, prismaService: PrismaService);
    validateUSer(username: string, password: string): Promise<UserDataWithoutPassword>;
    login(user: UserDataWithoutPassword): Promise<ReturnTokenInterface>;
    last_login(user: UserLastLoginDto): Promise<void>;
    refreshToken(refreshToken: string): Promise<{
        accessToken: string;
    }>;
    hash(password: string): string;
    CreateUser(inputUser: UserRegisterDto): Promise<void>;
    guestToken(payload: GuestPayloadInterface, res: Response): Promise<{
        guest_token: string;
    }>;
}
