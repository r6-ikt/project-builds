import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from "rxjs";
declare const GuestAuthGuard_base: import("@nestjs/passport").Type<import("@nestjs/passport").IAuthGuard>;
export declare class GuestAuthGuard extends GuestAuthGuard_base implements CanActivate {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean>;
}
export {};
