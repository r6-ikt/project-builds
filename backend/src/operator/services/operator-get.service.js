"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OperatorGetService = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
let OperatorGetService = class OperatorGetService {
    getAllOperatorsName() {
        try {
            return prisma.operators.findMany({
                select: {
                    name: true
                },
                where: {
                    active: true
                }
            });
        }
        catch (error) {
            throw new common_1.NotFoundException(error);
        }
    }
    getAllOperators() {
        try {
            return prisma.operators.findMany({
                where: {
                    active: true
                },
                orderBy: {
                    name: 'asc'
                }
            });
        }
        catch (error) {
            throw new common_1.NotFoundException(error);
        }
    }
    getOperator(id) {
        try {
            return prisma.operators.findMany({
                where: {
                    operator_id: {
                        equals: id,
                    }
                },
                select: {
                    name: true,
                }
            })[0];
        }
        catch (error) {
            throw new common_1.NotFoundException('You could not reach the database operators!');
        }
    }
};
exports.OperatorGetService = OperatorGetService;
exports.OperatorGetService = OperatorGetService = __decorate([
    (0, common_1.Injectable)()
], OperatorGetService);
//# sourceMappingURL=operator-get.service.js.map