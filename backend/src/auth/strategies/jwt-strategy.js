"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtStrategy = void 0;
const passport_1 = require("@nestjs/passport");
const passport_jwt_1 = require("passport-jwt");
const process = require("process");
class JwtStrategy extends (0, passport_1.PassportStrategy)(passport_jwt_1.Strategy, 'jwt') {
    constructor() {
        super({
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromExtractors([
                JwtStrategy.extractJWT,
                passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken()
            ]),
            ignoreExpiration: false,
            secretOrKey: `${process.env.jwt_secret}`
        });
    }
    async validate(payload) {
        return payload;
    }
    static extractJWT(req) {
        if (req.cookies && 'ac' in req.cookies) {
            return req.cookies.ac;
        }
        return null;
    }
}
exports.JwtStrategy = JwtStrategy;
//# sourceMappingURL=jwt-strategy.js.map