import { PrismaService } from "../../../prisma/prisma.service";
import { returnOperatorDto } from "../../dto/return-operator.dto";
import { createOperatorInput } from "../../controllers/operator/operator-create.controller";
export declare class OperatorCreateService {
    private prismaService;
    constructor(prismaService: PrismaService);
    createOperator(operatorCreateInput: createOperatorInput): Promise<returnOperatorDto>;
}
