import { SetInactiveUserService } from "../../services/users/set-inactive-user.service";
import { returnUserDto } from "../../dto/return-user.dto";
export declare class SetInactiveUserController {
    private readonly setInactiveUserService;
    constructor(setInactiveUserService: SetInactiveUserService);
    setInactiveUser(id: string): Promise<returnUserDto>;
}
