"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.returnOperatorDto = void 0;
const swagger_1 = require("@nestjs/swagger");
class returnOperatorDto {
}
exports.returnOperatorDto = returnOperatorDto;
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator azonosítója!", example: 1, type: Number }),
    __metadata("design:type", Number)
], returnOperatorDto.prototype, "operator_id", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator neve!", example: "Ace", type: String }),
    __metadata("design:type", String)
], returnOperatorDto.prototype, "name", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator oldala", example: true, type: Boolean }),
    __metadata("design:type", Boolean)
], returnOperatorDto.prototype, "side", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator élete!", example: 3, type: Number }),
    __metadata("design:type", Number)
], returnOperatorDto.prototype, "hp", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "AZ operator gyorsasága!", example: 1, type: Number }),
    __metadata("design:type", Number)
], returnOperatorDto.prototype, "speed", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator kiadásának éve!", example: 2016, type: Number }),
    __metadata("design:type", Number)
], returnOperatorDto.prototype, "release_date", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator nemzetisége!", example: "hungarian", type: String }),
    __metadata("design:type", String)
], returnOperatorDto.prototype, "nationality", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator egyik elsődleges fegyvere", example: "SMG", type: String }),
    __metadata("design:type", String)
], returnOperatorDto.prototype, "weapon1", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator másik elsődleges fegyvere", example: "Assault Rifle", type: String }),
    __metadata("design:type", String)
], returnOperatorDto.prototype, "weapon2", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({ required: true, description: "Az operator csapata", example: "Ghosteyes", type: String }),
    __metadata("design:type", String)
], returnOperatorDto.prototype, "squad", void 0);
//# sourceMappingURL=return-operator.dto.js.map