"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetOperatorHistoryController = void 0;
const common_1 = require("@nestjs/common");
const get_operator_history_service_1 = require("../../services/operator/get-operator-history.service");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const history_operator_dto_1 = require("../../dto/history-operator.dto");
const swagger_1 = require("@nestjs/swagger");
let GetOperatorHistoryController = class GetOperatorHistoryController {
    constructor(getOperatorHistoryService) {
        this.getOperatorHistoryService = getOperatorHistoryService;
    }
    getOperatorHistory() {
        return this.getOperatorHistoryService.getOperatorHistory();
    }
    async getItems(page, size, sort) {
        return this.getOperatorHistoryService.getOperatorHistoryByPages(size, page, sort);
    }
};
exports.GetOperatorHistoryController = GetOperatorHistoryController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: history_operator_dto_1.historyOperatorDto, isArray: true, description: "Visszaadja a history_operator tábla teljes tartalmát!" }),
    (0, common_1.Get)('/operator-history'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], GetOperatorHistoryController.prototype, "getOperatorHistory", null);
__decorate([
    (0, swagger_1.ApiResponse)({ status: 200, type: get_operator_history_service_1.HistoryOperatorResponseProps, description: "Oldalanként adja át az összes history_operator adatait!" }),
    (0, swagger_1.ApiParam)({ name: 'page', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'size', type: 'number', required: true }),
    (0, swagger_1.ApiQuery)({ name: 'sort', type: 'string', required: true }),
    (0, common_1.Get)('/operator-history/page/:page'),
    __param(0, (0, common_1.Param)('page')),
    __param(1, (0, common_1.Query)('size')),
    __param(2, (0, common_1.Query)('sort')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, String]),
    __metadata("design:returntype", Promise)
], GetOperatorHistoryController.prototype, "getItems", null);
exports.GetOperatorHistoryController = GetOperatorHistoryController = __decorate([
    (0, swagger_1.ApiBearerAuth)('accessToken'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'Operator'),
    (0, common_1.Controller)('admin'),
    __metadata("design:paramtypes", [get_operator_history_service_1.GetOperatorHistoryService])
], GetOperatorHistoryController);
//# sourceMappingURL=get-operator-history.controller.js.map