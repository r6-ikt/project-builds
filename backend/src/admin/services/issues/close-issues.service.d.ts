import { PrismaService } from "../../../prisma/prisma.service";
import { ReturnIssueDto } from "../../dto/return-issue.dto";
export declare class CloseIssuesService {
    private prismaService;
    constructor(prismaService: PrismaService);
    closeAllIssue(): Promise<void>;
    closeIssue(id: number): Promise<ReturnIssueDto>;
}
export declare function formatDate(date: Date): string;
export declare function formatDateWithOutTime(date: Date): string;
