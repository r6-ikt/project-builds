"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdatePatchNotesController = void 0;
const common_1 = require("@nestjs/common");
const update_patch_notes_service_1 = require("../../services/patchnotes/update-patch-notes/update-patch-notes.service");
const update_patch_notes_dto_1 = require("../../services/patchnotes/update-patch-notes/update-patch-notes.dto");
const is_admin_decorator_1 = require("../../../custom decorator/is-admin.decorator");
const swagger_1 = require("@nestjs/swagger");
let UpdatePatchNotesController = class UpdatePatchNotesController {
    constructor(updatePatchNoteService) {
        this.updatePatchNoteService = updatePatchNoteService;
    }
    async updatePatchNotes(updatePatchNotes) {
        return await this.updatePatchNoteService.updatePatchNotes(updatePatchNotes);
    }
};
exports.UpdatePatchNotesController = UpdatePatchNotesController;
__decorate([
    (0, swagger_1.ApiResponse)({ status: 201, description: "Frissíti a kivánt patch-note-ot!" }),
    (0, swagger_1.ApiBody)({ type: update_patch_notes_dto_1.UpdatePatchNotesDto, description: 'A patch-note frissítésének beviteli értéke!' }),
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [update_patch_notes_dto_1.UpdatePatchNotesDto]),
    __metadata("design:returntype", Promise)
], UpdatePatchNotesController.prototype, "updatePatchNotes", null);
exports.UpdatePatchNotesController = UpdatePatchNotesController = __decorate([
    (0, swagger_1.ApiBearerAuth)('access-token'),
    (0, is_admin_decorator_1.Admin)(),
    (0, swagger_1.ApiTags)('Admin', 'patch-notes'),
    (0, common_1.Controller)('update-patch-notes'),
    __metadata("design:paramtypes", [update_patch_notes_service_1.UpdatePatchNotesService])
], UpdatePatchNotesController);
//# sourceMappingURL=update-patch-notes.controller.js.map