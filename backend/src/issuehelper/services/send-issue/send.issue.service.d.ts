import { PrismaService } from "../../../prisma/prisma.service";
import { disabledUserDTO } from "../../dto/disabledUser.dto";
import { ReturnIssueInterface } from "../../dto/returnIssue.interface";
import { IssueHelperDto } from "../../dto/issueHelper.dto";
export declare class SendIssueService {
    private prismaService;
    constructor(prismaService: PrismaService);
    disabledUsers: disabledUserDTO[];
    sendIssue(issue: IssueHelperDto, req: any): Promise<ReturnIssueInterface>;
    expireDisable(): void;
}
